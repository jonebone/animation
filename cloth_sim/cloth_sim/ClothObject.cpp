///////////////////////////////////////////////////
//
// The below is a modified version of a class created by Hamish Carr
// Changes have been indicated in the comments
// and the original TexturedObject class he wrote is included in this project as TexturedObject.cpp and TexturedObject.h
//
///////////////////////////////////////////////////
//
//  Hamish Carr
//  September, 2020
//
//  ------------------------
//  TexturedObject.cpp
//  ------------------------
//  
//  Base code for rendering assignments.
//
//  Minimalist (non-optimised) code for reading and 
//  rendering an object file
//  
//  We will make some hard assumptions about input file
//  quality. We will not check for manifoldness or 
//  normal direction, &c.  And if it doesn't work on 
//  all object files, that's fine.
//
//  While I could set it up to use QImage for textures,
//  I want this code to be reusable without Qt, so I 
//  shall make a hard assumption that textures are in 
//  ASCII PPM and use my own code to read them
//  
///////////////////////////////////////////////////

// include the header file
#include "ClothObject.h"

// include the C++ standard libraries we want
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <cmath>

// include the Cartesian 3- vector class
#include "Cartesian3.h"

#define MAXIMUM_LINE_LENGTH 1024

// constructor will initialise to safe values
ClothObject::ClothObject()
    { // ClothObject()
        // add default vals for things
        clothDim = 15;
        centreOfGravity = Cartesian3(clothDim / 2.0f, 0.0, clothDim/2.0f);
        numPoints = clothDim * clothDim;
        // set point mass and dist to default 1
        pointMass = 0.1f;
        pointBaseDist = 1.0f;

        // init Ks and Kd
        Ks = 13.0f;
        Kd = 0.8f;

        sphereCenter = Cartesian3((float)clothDim / 2.0f, (float)clothDim / 4.0f, (float)clothDim / 2.0f);
        sphereRadius = (float)clothDim / 4.0f;

        // prepare mpVector to be the right size later
        mpVector.reserve(numPoints);


        // force arrays to size 0
        vertices.resize(0);
        normals.resize(0);
        textureCoords.resize(0);
} // ClothObject()

//////////////////////////////////////////////
/// Additional constructor with argument for cloth dimention
ClothObject::ClothObject(int squareDim) :
    clothDim(squareDim)
    {
        centreOfGravity = Cartesian3(clothDim / 2.0f, 0.0, clothDim/2.0f);
        numPoints = clothDim * clothDim;
        // set point mass and dist to default 1
        pointMass = 0.001;
        pointBaseDist = 1.0f;

        // prepare mpVector to be the right size later
        mpVector.reserve(numPoints);

        // force original arrays to size 0
        vertices.resize(0);
        normals.resize(0);
        textureCoords.resize(0);

}
    
//////////////////////////////////////////////

// read routine returns true on success, failure otherwise
bool ClothObject::ReadObjectStream(std::istream &geometryStream, std::istream &textureStream)
    { // ReadObjectStream()
    
    // create a read buffer
    char readBuffer[MAXIMUM_LINE_LENGTH];
    
    // the rest of this is a loop reading lines & adding them in appropriate places
    while (true)
        { // not eof
        // character to read
        char firstChar = geometryStream.get();
        
        // check for eof() in case we've run out
        if (geometryStream.eof())
            break;

        // otherwise, switch on the character we read
        switch (firstChar)
            { // switch on first character
            case '#':       // comment line
                // read and discard the line
                geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);
                break;
                
            case 'v':       // vertex data of some type
                { // some sort of vertex data
                // retrieve another character
                char secondChar = geometryStream.get();
                
                // bail if we ran out of file
                if (geometryStream.eof())
                    break;

                // now use the second character to choose branch
                switch (secondChar)
                    { // switch on second character
                    case ' ':       // space - indicates a vertex
                        { // vertex read
                        Cartesian3 vertex;
                        geometryStream >> vertex;
                        vertices.push_back(vertex);
                        break;
                        } // vertex read
                    case 'n':       // n indicates normal vector
                        { // normal read
                        Cartesian3 normal;
                        geometryStream >> normal;
                        normals.push_back(normal);
                        break;
                        } // normal read
                    case 't':       // t indicates texture coords
                        { // tex coord
                        Cartesian3 texCoord;
                        geometryStream >> texCoord;
                        textureCoords.push_back(texCoord);
                        break;                  
                        } // tex coord
                    default:
                        break;
                    } // switch on second character 
                break;
                } // some sort of vertex data
                
            case 'f':       // face data
                { // face
                // a face can have an arbitrary number of vertices, which is a pain
                // so we will create a separate buffer to read from
                geometryStream.getline(readBuffer, MAXIMUM_LINE_LENGTH);

                // turn into a C++ string
                std::string lineString = std::string(readBuffer);

                // create a string stream
                std::stringstream lineParse(lineString); 

                // create vectors for the IDs (with different names from the master arrays)
                std::vector<unsigned int> faceVertexSet;
                std::vector<unsigned int> faceNormalSet;
                std::vector<unsigned int> faceTexCoordSet;
                
                // now loop through the line
                while (!lineParse.eof())
                    { // lineParse isn't done
                    // the triple of vertex, normal, tex coord IDs
                    unsigned int vertexID;
                    unsigned int normalID;
                    unsigned int texCoordID;

                    // try reading them in, breaking if we hit eof
                    lineParse >> vertexID;
                    // retrieve & discard a slash
                    lineParse.get();
                    // check eof
                    if (lineParse.eof())
                        break;
                    
                    // and the tex coord
                    lineParse >> texCoordID;
                    lineParse.get();
                    if (lineParse.eof())
                        break;
                        
                    // read normal likewise
                    lineParse >> normalID;
                        
                    // if we got this far, we presumably have three valid numbers, so add them
                    // but notice that .obj uses 1-based numbering, where our arrays use 0-based
                    faceVertexSet.push_back(vertexID-1);
                    faceNormalSet.push_back(normalID-1);
                    faceTexCoordSet.push_back(texCoordID-1);
                    } // lineParse isn't done

                // as long as the face has at least three vertices, add to the master list
                if (faceVertexSet.size() > 2)
                    { // at least 3
                    faceVertices.push_back(faceVertexSet);
                    faceNormals.push_back(faceNormalSet);
                    faceTexCoords.push_back(faceTexCoordSet);
                    } // at least 3
                
                break;
                } // face
                
            // default processing: do nothing
            default:
                break;

            } // switch on first character

        } // not eof

    // compute centre of gravity
    // note that very large files may have numerical problems with this
    centreOfGravity = Cartesian3(0.0, 0.0, 0.0);

    // if there are any vertices at all
    if (vertices.size() != 0)
        { // non-empty vertex set
        // sum up all of the vertex positions
        for (unsigned int vertex = 0; vertex < vertices.size(); vertex++)
            centreOfGravity = centreOfGravity + vertices[vertex];
        
        // and divide through by the number to get the average position
        // also known as the barycentre
        centreOfGravity = centreOfGravity / vertices.size();

        // start with 0 radius
        objectSize = 0.0;

        // now compute the largest distance from the origin to a vertex
        for (unsigned int vertex = 0; vertex < vertices.size(); vertex++)
            { // per vertex
            // compute the distance from the barycentre
            float distance = (vertices[vertex] - centreOfGravity).length();         
            
            // now test for maximality
            if (distance > objectSize)
                objectSize = distance;
            } // per vertex
        } // non-empty vertex set

    // now read in the texture file
    texture.ReadPPM(textureStream);

    // return a success code
    return true;
    } // ReadObjectStream()

// write routine
void ClothObject::WriteObjectStream(std::ostream &geometryStream, std::ostream &textureStream)
    { // WriteObjectStream()
    // output the vertex coordinates
    for (unsigned int vertex = 0; vertex < vertices.size(); vertex++)
        geometryStream << "v  " << std::fixed << vertices[vertex] << std::endl;
    geometryStream << "# " << vertices.size() << " vertices" << std::endl;
    geometryStream << std::endl;

    // and the normal vectors
    for (unsigned int normal = 0; normal < normals.size(); normal++)
        geometryStream << "vn " << std::fixed << normals[normal] << std::endl;
    geometryStream << "# " << normals.size() << " vertex normals" << std::endl;
    geometryStream << std::endl;

    // and the texture coordinates
    for (unsigned int texCoord = 0; texCoord < textureCoords.size(); texCoord++)
        geometryStream << "vt " << std::fixed << textureCoords[texCoord] << std::endl;
    geometryStream << "# " << textureCoords.size() << " texture coords" << std::endl;
    geometryStream << std::endl;

    // and the faces
    for (unsigned int face = 0; face < faceVertices.size(); face++)
        { // per face
        geometryStream << "f ";
        
        // loop through # of vertices
        for (unsigned int vertex = 0; vertex < faceVertices[face].size(); vertex++)
            geometryStream << faceVertices[face][vertex]+1 << "/" << faceTexCoords[face][vertex]+1 << "/" << faceNormals[face][vertex]+1 << " " ;
        
        geometryStream << std::endl;
        } // per face
    geometryStream << "# " << faceVertices.size() << " polygons" << std::endl;
    geometryStream << std::endl;
    
    // now output the texture
    texture.WritePPM(textureStream);
    } // WriteObjectStream()

// routine to transfer assets to GPU
void ClothObject::TransferAssetsToGPU() { // TransferAssetsToGPU()
    // when this is called, it transfers assets to the GPU.
    // for now, it will only be to transfer the texture
    // this may not be efficient, but it supports arbitrary sizes best
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    // create a texture ID (essentially a pointer)
    glGenTextures(1, &textureID);
    // now bind to it - i.e. all following code addresses this one
    glBindTexture(GL_TEXTURE_2D, textureID);
    // set these parameters to avoid dealing with mipmaps 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    // now transfer the image
    glTexImage2D(   
        GL_TEXTURE_2D,      // it's a 2D texture
        0,                  // mipmap level of 0 (ie the largest one)
        GL_RGBA,            // we want the data stored as RGBA on GPU
        texture.width,      // width of the image
        texture.height,     // height of the image
        0,                  // width of border (in texels)
        GL_RGBA,            // format the data is stored in on CPU
        GL_UNSIGNED_BYTE,   // data type
        texture.block       // and a pointer to the data
        );
} // TransferAssetsToGPU()

// routine to render
void ClothObject::Render(RenderParameters *renderParameters) { // Render()
    // Ideally, we would apply a global transformation to the object, but sadly that breaks down
    // when we want to scale things, as unless we normalise the normal vectors, we end up affecting
    // the illumination.  Known solutions include:
    // 1.   Normalising the normal vectors
    // 2.   Explicitly dividing the normal vectors by the scale to balance
    // 3.   Scaling only the vertex position (slower, but safer)
    // 4.   Not allowing spatial zoom (note: sniper scopes are a modified projection matrix)
    //
    // Inside a game engine, zoom usually doesn't apply. Normalisation of normal vectors is expensive,
    // so we will choose option 2.  

    // if we have texturing enabled . . . 
    if (renderParameters->texturedRendering)
        { // textures enabled
        // enable textures
        glEnable(GL_TEXTURE_2D);
        // use our other flag to specify replace or modulate
        if (renderParameters->textureModulation)
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        else
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        // now bind the texture ID
        glBindTexture(GL_TEXTURE_2D, textureID);
        } // textures enabled
    else
        { // textures disabled
        // make sure that they are disabled
        glDisable(GL_TEXTURE_2D);
        } // textures disabled

    // Scale defaults to the zoom setting
    float scale = renderParameters->zoomScale;
    
    // if object scaling is requested, apply it as well 
    if (renderParameters->scaleObject)
        scale /= objectSize;
        
    //  now scale everything
//     glScalef(scale, scale, scale);
    glTranslatef(0.0, centreOfGravity.y * scale, 0.0);
    // apply the translation to the centre of the object if requested
    if (renderParameters->centreObject)
        glTranslatef(-centreOfGravity.x * scale, -centreOfGravity.y * scale, -centreOfGravity.z * scale);

    // emissive glow from object
    float emissiveColour[4];
    // default ambient / diffuse / specular colour
    float surfaceColour[4] = { 0.7, 0.7, 0.7, 1.0 };
    // specular shininess
    float shininess[4];
    // copy the intensity into RGB channels
    emissiveColour[0]   = emissiveColour[1] = emissiveColour[2] = renderParameters->emissiveLight;
    emissiveColour[3]   = 1.0; // don't forget alpha
    
    // set the shininess from the specular exponent
    shininess[0]        = shininess[1]      = shininess[2]      = renderParameters->specularExponent;
    shininess[3]        = 1.0; // alpha

    // start rendering
    glBegin(GL_TRIANGLES);

    // we assume a single material for the entire object
    glMaterialfv(GL_FRONT, GL_EMISSION, emissiveColour);
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, surfaceColour);
    glMaterialfv(GL_FRONT, GL_SPECULAR, surfaceColour);
    glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
    
    // repeat this for colour - extra call, but saves if statements
    glColor3fv(surfaceColour);

    // loop through the faces: note that they may not be triangles, which complicates life
    for (unsigned int face = 0; face < faceVertices.size(); face++)
        { // per face
        // on each face, treat it as a triangle fan starting with the first vertex on the face
        for (unsigned int triangle = 0; triangle < faceVertices[face].size() - 2; triangle++)
            { // per triangle
            // now do a loop over three vertices
            for (unsigned int vertex = 0; vertex < 3; vertex++)
                { // per vertex
                // we always use the face's vertex 0
                int faceVertex = 0;
                // so if it isn't 0, we want to add the triangle base ID
                if (vertex != 0)
                    faceVertex = triangle + vertex;

                // now we use that ID to lookup
                glNormal3f
                    (
                    normals         [faceNormals    [face][faceVertex]  ].x,
                    normals         [faceNormals    [face][faceVertex]  ].y,
                    normals         [faceNormals    [face][faceVertex]  ].z
                    );
                    
                // if we're using UVW colours, set both colour and material
                if (renderParameters->mapUVWToRGB)
                    { // set colour and material
                    float *colourPointer = (float *) &(textureCoords[faceTexCoords[face][faceVertex]]);
                    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, colourPointer);
                    glMaterialfv(GL_FRONT, GL_SPECULAR, colourPointer);
                    glColor3fv(colourPointer);
                    } // set colour and material
                // set the texture coordinate
                glTexCoord2f
                    (
                    textureCoords   [faceTexCoords  [face][faceVertex]  ].x,
                    textureCoords   [faceTexCoords  [face][faceVertex]  ].y
                    );
                    
                // and set the vertex position
                glVertex3f
                    (
                    scale * vertices        [faceVertices   [face][faceVertex]].x,
                    scale * vertices        [faceVertices   [face][faceVertex]].y,
                    scale * vertices        [faceVertices   [face][faceVertex]].z
                    );
                } // per vertex
            } // per triangle
        } // per face

    // close off the triangles
    glEnd();

    // if we have texturing enabled, turn texturing back off 
    if (renderParameters->texturedRendering)
        glDisable(GL_TEXTURE_2D);
} // Render()

// routine for students to use when rendering

//////////////////////////////////////////////
/// Additions to class methods

// cloth init (just populates the vector)
void ClothObject::InitializeCloth(){
    for (int r = 0; r < clothDim; r++)
        for (int c = 0; c < clothDim; c++ ) {
            MassPoint mp;
            // set IDs
            mp.id = r * clothDim + c;
            mp.rowID = r;
            mp.colID = c;

            // set position
            mp.pos = Cartesian3(c, (float)clothDim + 2.0f, r);

            // set velocity
            mp.velocity = Cartesian3();

            // set to having no forces
            mp.force = Cartesian3();

            // put in vector
            mpVector[mp.id] = mp;
        }
    // sim init things
    ResetForcesToGravity();
    AddAllSpringForces();
} // InitializeCloth()


Cartesian3 ClothObject::PosAt(int row, int col) {
    return mpVector[row * clothDim + col].pos;
}

////// SIM STUFF

void ClothObject::ResetForcesToGravity() {
    for (int i = 0; i < numPoints; i ++) {
        mpVector[i].force = mpVector[i].force + Cartesian3(0.0f, -9.8f, 0.0f) * pointMass;
    }
} // ResetForcesToGravity

Cartesian3 ClothObject::SpringForceBetween(int row1, int col1, int row2, int col2) {
    if (row2 < 0 || col2 < 0 || row2 >= clothDim || col2 >= clothDim) {
        //std::cout << row2 << " " << col2 << std::endl;
        return Cartesian3(); // if out of bounds, no force
    }
        

    MassPoint m1 = mpVector[row1 * clothDim + col1];
    MassPoint m2 = mpVector[row2 * clothDim + col2];
    //MassPoint m2 = mpVector[row1 * clothDim + col1];
    //MassPoint m1 = mpVector[row2 * clothDim + col2];

    float newLength = (m2.pos - m1.pos).length();

    float restLength = Cartesian3(pointBaseDist * (row2 - row1), 0.0f, pointBaseDist * (col2 - col1)).length();

    Cartesian3 forceDir = (m2.pos - m1.pos)/newLength;
    
        
    Cartesian3 f = forceDir * ( (Ks * (newLength - restLength)) + Kd * ((m2.velocity - m1.velocity)).dot(forceDir) );

    if (row1 == 1 && row2 == 0 && col1 == 1 && col2 == 0) {
        //std::cout << newLength - restLength << std::endl;
        //std::cout << forceDir << std::endl;
        //std::cout << f << std::endl;
    }

    return f;

} // SpringForceBetween

void ClothObject::AddSpringForcesOn(int row, int col) {
    Cartesian3 forces = Cartesian3();
    // row - 1
    forces = forces + SpringForceBetween(row, col, row - 1, col - 1);
    forces = forces + SpringForceBetween(row, col, row - 1, col);
    forces = forces + SpringForceBetween(row, col, row - 1, col + 1);
    // row
    forces = forces + SpringForceBetween(row, col, row, col - 1);
    //forces = forces + SpringForceBetween(row, col, row, col); // no spring from self to self
    forces = forces + SpringForceBetween(row, col, row, col + 1);
    // row + 1
    forces = forces + SpringForceBetween(row, col, row + 1, col - 1);
    forces = forces + SpringForceBetween(row, col, row + 1, col);
    forces = forces + SpringForceBetween(row, col, row + 1, col + 1);

    mpVector[row * clothDim + col].force = mpVector[row * clothDim + col].force + forces;
}

void ClothObject::AddAllSpringForces() {
    for (int r = 0; r < clothDim; r++)
        for (int c = 0; c < clothDim; c ++)
            AddSpringForcesOn(r, c);
}

void ClothObject::UpdatePositionsFor(float deltaT, RenderParameters *renderParameters) {
    

    for (int i = 0; i < numPoints; i++) {
        // calculate acceleration per MassPoint
        Cartesian3 acceleration = mpVector[i].force/pointMass;
        // update velocity per point
        mpVector[i].velocity = mpVector[i].velocity + acceleration * deltaT;
        //mpVector[i].velocity = acceleration * deltaT;
        // update position based on velocity
        mpVector[i].pos = mpVector[i].pos + mpVector[i].velocity * deltaT;

        // init friction forces
        Cartesian3 frictionForce = Cartesian3();
        Cartesian3 frictionDirection = Cartesian3();

        // floor collision
        if (mpVector[i].pos.y < 0 ) {
            

            float normalForce = -floorNormal.dot(mpVector[i].force);
            
            mpVector[i].pos.y = -mpVector[i].pos.y * restitution;
            mpVector[i].velocity.y = -mpVector[i].velocity.y * restitution;  
            // add floor friction
            // friction direction should be generally opposite the velocity but exactly perpendicular to the normal of the surface
            frictionDirection = floorNormal.cross(floorNormal.cross(mpVector[i].velocity.unit()));
            frictionForce = frictionDirection * normalForce * floorFriction;
        }

        // sphere collision
        Cartesian3 sphereVector = mpVector[i].pos - sphereCenter;
        if (sphereVector.length() < sphereRadius 
            && renderParameters->phongShadingOn // repurposing render parameters
            ) {
            Cartesian3 sphereNormal = sphereVector.unit();
            

            float normalForce = -sphereNormal.dot(mpVector[i].force);
            
            mpVector[i].pos = mpVector[i].pos + sphereNormal * (sphereRadius - sphereVector.length()) * sphereRestitution;
            mpVector[i].velocity = mpVector[i].velocity - sphereNormal * sphereNormal.dot(mpVector[i].velocity) * (1.f + sphereRestitution);  
            
            // add sphere friction
            // friction direction should be generally opposite the velocity but exactly perpendicular to the normal of the surface
            frictionDirection = sphereNormal.cross(sphereNormal.cross(mpVector[i].velocity.unit())).unit();
            frictionForce = frictionForce + frictionDirection * normalForce * sphereFriction;
        }
        
        // reset forces to 0
        mpVector[i].force = Cartesian3();

        // add friction forces for next step
        mpVector[i].force = mpVector[i].force + frictionForce;
        // yes this is less efficient to do in two steps but not by much
        // but it more clearly represents the concept behind it

    }
    //std::cout << mpVector[24].force << std::endl;
} 

void ClothObject::SimStep(float deltaT, RenderParameters *renderParameters) {
    
    if (renderParameters->mapUVWToRGB) { // repurposing the render params
        // excluding two corners by setting their forces to zero
        mpVector[0].force = mpVector[clothDim - 1].force = Cartesian3();
        mpVector[0].velocity = mpVector[clothDim - 1].velocity = Cartesian3();
    }
    

    UpdatePositionsFor(deltaT, renderParameters);
    // calculate new forces right after updates so that they don't need to be called before updating positions again
    ResetForcesToGravity();
    AddAllSpringForces();

} 

// RENDERING

// rendering cloth
void ClothObject::RenderCloth(RenderParameters *renderParameters) {
    glColor3f(1.f, 1.f, 1.f);
    glPointSize(10.0f);
    glLineWidth(2.0f);

    // scale stuff (from original Render)
    // Scale defaults to the zoom setting
    float scale = renderParameters->zoomScale;
    
    // if object scaling is requested, apply it as well 
    if (renderParameters->scaleObject) {
        objectSize = clothDim / 3.0f;
        scale /= objectSize;
    }
    //  now scale everything
    glScalef(scale, scale, scale);
    // apply the translation to the centre of the object if requested
    if (renderParameters->centreObject)
        glTranslatef(-centreOfGravity.x * scale, -centreOfGravity.y * scale, -centreOfGravity.z * scale);
    // end scale stuff

    // draw sphere in if appropriate
    if (renderParameters->phongShadingOn) {
        glPushMatrix();
        glColor3f(0.5f, 0.5f, 0.5f);
        glTranslatef(sphereCenter.x, sphereCenter.y, sphereCenter.z);
        this->sphere();
        glPopMatrix();
        glColor3f(1.f, 1.f, 1.f);
    }

    // init position values // optimize later
    //Cartesian3 selfPos = PosAt(0,0);
    Cartesian3 rightPos, downPos, diagDRPos, diagDLPos;
    
    for (int r = 0; r < clothDim; r++)
        for (int c = 0; c < clothDim; c++) {
            Cartesian3 selfPos = PosAt(r,c);
            glBegin(GL_POINTS);
                glVertex3f(selfPos.x, selfPos.y, selfPos.z);
            glEnd();


            glBegin(GL_LINES);

            Cartesian3 velo = mpVector[r * clothDim + c].velocity;
            Cartesian3 f = mpVector[r * clothDim + c].force;
            glColor3f(0.3, 0.7, 0.3);
            //glVertex3f(selfPos.x, selfPos.y, selfPos.z);
            //glVertex3f(selfPos.x + velo.x, selfPos.y + velo.y, selfPos.z + velo.z);
            glColor3f(1.f, 1.f, 1.f);
            glColor3f(0.7, 0.3, 0.3);
            //glVertex3f(selfPos.x, selfPos.y, selfPos.z);
            //glVertex3f(selfPos.x + f.x, selfPos.y + f.y, selfPos.z + f.z);
            glColor3f(1.f, 1.f, 1.f);

            if (r < clothDim - 1) {
                downPos = PosAt(r + 1, c);
                
                // line down 
                glVertex3f(selfPos.x, selfPos.y, selfPos.z);
                glVertex3f(downPos.x, downPos.y, downPos.z);
                
                // diagonals
                if (c > 0) {
                    diagDLPos = PosAt(r + 1, c - 1);
                    // line diag down left
                    glVertex3f(selfPos.x, selfPos.y, selfPos.z);
                    glVertex3f(diagDLPos.x, diagDLPos.y, diagDLPos.z);
                }
                if (c < clothDim - 1) {
                    diagDRPos = PosAt(r + 1, c + 1);
                    // line diag down right
                    glVertex3f(selfPos.x, selfPos.y, selfPos.z);
                    glVertex3f(diagDRPos.x, diagDRPos.y, diagDRPos.z);

                    // can also do right while we're here
                    rightPos = PosAt(r, c + 1);
                    // line to right
                    glVertex3f(selfPos.x, selfPos.y, selfPos.z);
                    glVertex3f(rightPos.x, rightPos.y, rightPos.z);
                } 
            }
            else if (c < clothDim - 1) {
                downPos = PosAt(r, c + 1);
                // line down
                glVertex3f(selfPos.x, selfPos.y, selfPos.z);
                glVertex3f(downPos.x, downPos.y, downPos.z);
            }
            glEnd();
        }

    if (renderParameters->showObject)
        for (int r = 0; r < clothDim - 1; r++)
            for (int c = 0; c < clothDim - 1; c++) {
                //draw quads
            }

}

// sphere drawing method
void ClothObject::sphere(){
    float PI = 3.1415926535;
    float r = sphereRadius * 0.95; // scale down slightly so cloth lines show on surface
    // sections per ring of the sphere
    float d = 50;
    //r = 1;

    float x0, x1, x2, x3, y0, y1, y2, y3, z0, z1;

    x1 = 0;
    y1 = 0;
    z1 = r;

    for (int theta_step = 0; theta_step < d; theta_step++){
        z0 = r * cos((theta_step)*PI/d);
        z1 = r * cos((theta_step + 1)*PI/d);

        for(int phi_step = 0; phi_step < d; phi_step++){
            x0 = r * cos(2*phi_step*PI/d) * sin(theta_step*PI/d);
            x1 = r * cos(2*(phi_step+1)*PI/d) * sin(theta_step*PI/d);
            x2 = r * cos(2*(phi_step)*PI/d) * sin((theta_step+1)*PI/d);
            x3 = r * cos(2*(phi_step+1)*PI/d) * sin((theta_step+1)*PI/d);
            y0 = r * sin(2*phi_step*PI/d) * sin(theta_step*PI/d);
            y1 = r * sin(2*(phi_step + 1)*PI/d) * sin(theta_step*PI/d);
            y2 = r * sin(2*(phi_step)*PI/d) * sin((theta_step + 1)*PI/d);
            y3 = r * sin(2*(phi_step + 1)*PI/d) * sin((theta_step + 1)*PI/d);


            glBegin(GL_POLYGON);
            glVertex3f(x0,y0,z0);
            glNormal3f(x0,y0,z0);
            glVertex3f(x1,y1,z0);
            glNormal3f(x1,y1,z0);
            glVertex3f(x3,y3,z1);
            glNormal3f(x3,y3,z1);
            glVertex3f(x2,y2,z1);
            glNormal3f(x2,y2,z1);
            glEnd();
        }
    }

}


/// end additions to class methods
//////////////////////////////////////////////

