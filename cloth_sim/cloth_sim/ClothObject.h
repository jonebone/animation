///////////////////////////////////////////////////
//
// The below is a modified version of a class created by Hamish Carr
// Changes have been indicated in the comments
// and the original TexturedObject class he wrote is included in this project as TexturedObject.cpp and TexturedObject.h
//
///////////////////////////////////////////////////
//
//  Hamish Carr
//  September, 2020
//  
//  ------------------------
//  TexturedObject.h
//  ------------------------
//  
//  Base code for rendering assignments.
//
//  Minimalist (non-optimised) code for reading and 
//  rendering an object file
//  
//  We will make some hard assumptions about input file
//  quality. We will not check for manifoldness or 
//  normal direction, &c.  And if it doesn't work on 
//  all object files, that's fine.
//
//  While I could set it up to use QImage for textures,
//  I want this code to be reusable without Qt, so I 
//  shall make a hard assumption that textures are in 
//  ASCII PPM and use my own code to read them
//  
///////////////////////////////////////////////////

// include guard for ClothObject
#ifndef _CLOTH_OBJECT_H
#define _CLOTH_OBJECT_H

// include the C++ standard libraries we need for the header
#include <vector>
#include <iostream>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


// include the unit with Cartesian 3-vectors
#include "Cartesian3.h"
// the render parameters
#include "RenderParameters.h"
// the image class for a texture
#include "RGBAImage.h" 

class ClothObject { // class ClothObject
    public:

    //////////////////////////////////////////////
    /// Addition of masspoint struct
    

    struct MassPoint {
        // ID
        int id;

        // row/col info
        int rowID, colID;

        // position
        Cartesian3 pos;
        
        // aka overdot P value (starts as 0)
        Cartesian3 velocity;

        Cartesian3 force;
    };
    

    /// end addition
    //////////////////////////////////////////////

    // vector of vertices
    std::vector<Cartesian3> vertices;

    // vector of normals
    std::vector<Cartesian3> normals;
    
    // vector of texture coordinates (stored as triple to simplify code)
    std::vector<Cartesian3> textureCoords;

    // vector of faces
    std::vector<std::vector<unsigned int> > faceVertices;

    // corresponding vector of normals
    std::vector<std::vector<unsigned int> > faceNormals;
    
    // corresponding vector of texture coordinates
    std::vector<std::vector<unsigned int> > faceTexCoords;

    // RGBA Image for storing a texture
    RGBAImage texture;

    // a variable to store the texture's ID on the GPU
    GLuint textureID;

    // centre of gravity - computed after reading
    Cartesian3 centreOfGravity;

    // size of object - i.e. radius of circumscribing sphere centred at centre of gravity
    float objectSize;

    //////////////////////////////////////////////
    /// Additions to class properties

    // num points
    int numPoints, clothDim;
    
    // point data
    float pointMass, pointBaseDist, Ks, Kd;

    // physics stuff
    float restitution = 0.2;
    Cartesian3 floorNormal = Cartesian3(0., 1., 0.);
    float floorFriction = 0.7;

    // sphere facts
    float sphereRestitution = 0.1;
    float sphereFriction = 0.8;
    Cartesian3 sphereCenter;
    float sphereRadius;

    // vector of MassPoints
    std::vector<MassPoint> mpVector;

    /// end additions to class properties
    //////////////////////////////////////////////

    // constructor will initialise to safe values
    ClothObject();

    //////////////////////////////////////////////
    /// Additional constructor with argument for cloth dimention
    ClothObject(int squareDim);
    
    //////////////////////////////////////////////
    
    // read routine returns true on success, failure otherwise
    bool ReadObjectStream(std::istream &geometryStream, std::istream &textureStream);

    // write routine
    void WriteObjectStream(std::ostream &geometryStream, std::ostream &textureStream);

    // routine to transfer assets to GPU
    void TransferAssetsToGPU();
    
    // routine to render
    void Render(RenderParameters *renderParameters);

    //////////////////////////////////////////////
    /// Additions to class methods

    // cloth init (just populates the vector)
    void InitializeCloth();

    // getting masspoint data
    Cartesian3 PosAt(int row, int col);

    

    // render cloth
    void RenderCloth(RenderParameters *renderParameters);
    // sphere helper func
    void sphere();

    // sim stuff
    void ResetForcesToGravity();

    Cartesian3 SpringForceBetween(int row1, int col1, int row2, int col2);

    void AddSpringForcesOn(int row, int col);

    void AddAllSpringForces();

    void UpdatePositionsFor(float deltaT, RenderParameters *renderParameters);

    void SimStep(float deltaT, RenderParameters *renderParameters);



    /// end additions to class methods
    //////////////////////////////////////////////

}; // class ClothObject

// end of include guard for ClothObject
#endif
