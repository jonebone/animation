#include "kinematicWindow.h"

// constructor / destructor
kinematicWindow::kinematicWindow(QWidget *parent)
	: QWidget(parent)
	{ // constructor

	// create menu bar
	menuBar = new QMenuBar(this);
	
	// create file menu
	fileMenu = menuBar->addMenu("&File");

	// create the action
	actionQuit = new QAction("&Quit", this);

	// leave signals & slots to the controller
	
	// add the item to the menu
	fileMenu->addAction(actionQuit);
	
	// create the window layout
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
	// create sublayouts
	viewLayout = new QBoxLayout(QBoxLayout::LeftToRight);

	// create shared BVH
	bvh = new BVH();
	bvh->Load("bvh_files/walking.bvh");
	// create main widget
	mainViewWidget = new kinematicWidget(this, bvh);
	skeletonViewWidget = new kinematicWidget(this, bvh);
	skeletonViewWidget->flatViewEnable();
	
	viewLayout->addWidget(mainViewWidget, 2);
	viewLayout->addWidget(skeletonViewWidget, 1);
	// add sub layouts to parent layout after adding items to sub layout
	windowLayout->addLayout(viewLayout);

	// create slider
	nVerticesSlider = new QSlider(Qt::Horizontal);
	windowLayout->addWidget(nVerticesSlider);


	ptimer = new QTimer(this);
	ptimer->start(20);

	connect(ptimer, SIGNAL(timeout()),  mainViewWidget, SLOT(updateTime()));
	connect(ptimer, SIGNAL(timeout()),  skeletonViewWidget, SLOT(updateTime()));
	connect(mainViewWidget, SIGNAL(jKeyPressed()),  mainViewWidget, SLOT(jointModeUpdate()));
	connect(mainViewWidget, SIGNAL(pKeyPressed()),  mainViewWidget, SLOT(playpauseUpdate()));
	connect(mainViewWidget, SIGNAL(arrowKeyPressed(int)),  mainViewWidget, SLOT(updateHighlight(int)));
	

} // constructor

kinematicWindow::~kinematicWindow() { // destructor
	delete ptimer;
	delete nVerticesSlider;
	delete mainViewWidget;
	delete skeletonViewWidget;
	delete windowLayout;
	delete actionQuit;
	delete fileMenu;
	delete menuBar;
} // destructor

// resets all the interface elements
void kinematicWindow::ResetInterface() { // ResetInterface()
	nVerticesSlider->setMinimum(3);
	nVerticesSlider->setMaximum(30);

	//don't use the slider for now

	//	nVerticesSlider->setValue(thePolygon->nVertices);
	
	// now force refresh
	mainViewWidget->update();
	update();
} // ResetInterface()


void kinematicWindow::keyPressEvent(QKeyEvent *event) {
	if (event->key() == Qt::Key_J) {
		emit mainViewWidget->jKeyPressed();
	}
	else if (event->key() == Qt::Key_P) {
		emit mainViewWidget->pKeyPressed();
	}
	// arrow keys
	else if (event->key() == Qt::Key_Left)  emit mainViewWidget->arrowKeyPressed( 1);
	else if (event->key() == Qt::Key_Right) emit mainViewWidget->arrowKeyPressed( 2);
	else if (event->key() == Qt::Key_Up)    emit mainViewWidget->arrowKeyPressed(-1);
	else if (event->key() == Qt::Key_Down)  emit mainViewWidget->arrowKeyPressed( 0);
}
