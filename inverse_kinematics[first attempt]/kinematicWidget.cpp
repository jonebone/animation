// to run on mac: use OpenGl/glu.h
#include <OpenGl/glu.h>
// to run on Dec10: use GL/glu.h
//#include <GL/glu.h>
#include <QGLWidget>
#include "kinematicWidget.h"

#define BVH_SOURCE_FILE = "bvh_files/walking.bvh"

// Setting up material properties
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;


static materialStruct redPlasticMaterials = {
  {0.3, 0.0, 0.0, 1.0},
  {0.3, 0.0, 0.0, 1.0},
  {0.4, 0.2, 0.2, 1.0},
  32.0
};


static materialStruct brassMaterials = {
  { 0.33, 0.22, 0.03, 1.0},
  { 0.78, 0.57, 0.11, 1.0},
  { 0.99, 0.91, 0.81, 1.0},
  27.8 
};

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0 
};


// constructor
kinematicWidget::kinematicWidget(QWidget *parent):
  QGLWidget(parent),
  _time(0.0),
  jointModeActive(false),
  playingActive(true),
  flatViewEnabled(false)
  { // constructor
    bvh = new BVH();
    bvh->Load("bvh_files/walking.bvh");

} // constructor

// constructor
kinematicWidget::kinematicWidget(QWidget *parent, BVH *sharedBVH):
  QGLWidget(parent),
  _time(0.0),
  jointModeActive(false),
  playingActive(true)
  { // constructor
    bvh = sharedBVH;

} // constructor

// enable Flat view (for the skeleton view)
void kinematicWidget::flatViewEnable() {
  flatViewEnabled = true;
}


// called when OpenGL context is set up
void kinematicWidget::initializeGL() { // initializeGL()
	// set the widget background colour
	glClearColor(0.3, 0.3, 0.3, 0.0);
  //if (!bvh->IsLoadSuccess()) exit(EXIT_FAILURE);

} // initializeGL()


// called every time the widget is resized
void kinematicWidget::resizeGL(int w, int h) { // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);

	glEnable(GL_LIGHTING); // enable lighting in general
        glEnable(GL_LIGHT0);   // each light source must also be enabled

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

        
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glOrtho(-4.0, 4.0, -4.0, 4.0, -4.0, 4.0);

// SHOULDN'T NEED TO USE BOTH ORTHO AND PERSPECTIVE (gluLookAt)

  float aspectRatio = (float) w / (float) h;
  float orthoVal = 4.0f;
  if (flatViewEnabled) orthoVal = 3.0f;
  if (w >= h) glOrtho(-1.0 * aspectRatio * orthoVal, orthoVal * aspectRatio, -1.0 * orthoVal, orthoVal, -4.0, 4.0);
  else glOrtho(-1.0 * orthoVal, orthoVal, -1.0 * aspectRatio * orthoVal, orthoVal * aspectRatio, -4.0, 4.0);;

} // resizeGL()


void kinematicWidget::floor(){
  // create one face with brassMaterials
  materialStruct* p_front = &redPlasticMaterials;
  GLfloat floor = -0.01;
	
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  glNormal3f(0.,1.,0.);
  glBegin(GL_POLYGON);
    glVertex3f( -10.0,  floor,  10.0);
    glVertex3f(  10.0,  floor,  10.0);
    glVertex3f(  10.0,  floor, -10.0);
    glVertex3f( -10.0,  floor, -10.0);
  glEnd();

}

void kinematicWidget::cube(bool b_shadow){

  // Here are the normals, correctly calculated for the cube faces below  
  GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0, 1, 0}, {0, -1, 0} };

  if (b_shadow == true){
        glDisable(GL_LIGHTING);
        glColor3f(0.,0.,0.);
   }

  // create one face with brassMaterials
  materialStruct* p_front = &brassMaterials;
	
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

 
  glNormal3fv(normals[0]);
  glBegin(GL_POLYGON);
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f( 1.0,  1.0,  1.0);
  glEnd();

  // and the others white
  p_front = &whiteShinyMaterials;
	
  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);


  glNormal3fv(normals[3]); 
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();

 
  glNormal3fv(normals[2]); 
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  
  glNormal3fv(normals[1]);
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
  glEnd();

  
  glNormal3fv(normals[4]);
  glBegin(GL_POLYGON);
    glVertex3f(  1.0,  1.0,  1.0);
    glVertex3f(  1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
  glEnd();

 
  glNormal3fv(normals[5]);
  glBegin(GL_POLYGON);
    glVertex3f(  1.0,  -1.0,  1.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glVertex3f( -1.0,  -1.0,  1.0);
  glEnd();


    glEnable(GL_LIGHTING);
}

/// *********************************************************************** ///
/// **************************  SLOTS  ************************************ ///
/// *********************************************************************** ///


void kinematicWidget::updateTime(){
  if (playingActive) _time += 1.0;
  this->repaint();
}	

void kinematicWidget::playpauseUpdate(){
  if (playingActive) {
    playingActive = false;
  } else {
    playingActive = true;
    _time += 1.0;
  }
    
  this->repaint();
}

void kinematicWidget::jointModeUpdate(){
  if (jointModeActive) {
    jointModeActive = false;
    bvh->joint_highlight_enabled = false;
  } else {
    jointModeActive = true;
    bvh->joint_highlight_enabled = true;
    bvh->initJointMode();
  }
  if (playingActive)
    _time += 1.0;
  this->repaint();
}

void kinematicWidget::updateHighlight(int dir){
  if (jointModeActive) {
    bvh->shiftHighlight(dir);
  }
  if (playingActive) _time += 1.0;
  this->repaint();
}



/// *********************************************************************** ///
/// **************************  PAINT  ************************************ ///
/// *********************************************************************** ///

// called every time the widget needs painting
void kinematicWidget::paintGL()
	{ // paintGL()
	// clear the widget
       	glEnable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Position light
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
  
	GLfloat light_pos[] = {1.5, 7, 1., 1.};
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
	glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,180.);

	glPopMatrix();
	//this->floor();
  

	// Done light

	// You must set the matrix mode to model view directly before enabling the depth test
      	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

  bvh->RenderFigure((int)_time % bvh->num_frame, 0.075f);
	

	glPopMatrix();

  //glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
  if (flatViewEnabled) gluLookAt( -1.0,0.75,2.0, 
                                  -1.0,0.75,-2.0, 
                                  0.0,1.0,0.0);
  else	gluLookAt(1.5,1.5,1.5, 
                  3.0,3.0,-3.0, 
                  0.0,1.0,0.0);
	
	// flush to screen
	glFlush();	

	} // paintGL()
