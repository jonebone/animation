#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QTimer>
#include <QBoxLayout>
#include <QEvent>
#include <QKeyEvent>
#include <iostream>
#include "kinematicWidget.h"

class kinematicWindow: public QWidget { 
	public:
       
	
	// constructor / destructor
	kinematicWindow(QWidget *parent);
	~kinematicWindow();

	// visual hierarchy
	// menu bar
	QMenuBar *menuBar;
		// file menu
		QMenu *fileMenu;
			// quit action
			QAction *actionQuit;

	// window layout
	QBoxLayout *windowLayout;
	QBoxLayout *viewLayout;

	// the shared BVH instance
	BVH *bvh;
	// beneath that, the openGL view widgets
	kinematicWidget *mainViewWidget;
	kinematicWidget *skeletonViewWidget;

	// and other UI sliders
	QSlider *nVerticesSlider;

	// a timer
	QTimer *ptimer;

	void keyPressEvent(QKeyEvent *event);

	// resets all the interface elements
	void ResetInterface();

}; 
	
#endif
