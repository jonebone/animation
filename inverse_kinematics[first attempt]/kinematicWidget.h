#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <QKeyEvent>
#include <iostream>
#include "BVH.h"

class kinematicWidget: public QGLWidget
	{ // 

	Q_OBJECT

	public:
	kinematicWidget(QWidget *parent);
	kinematicWidget(QWidget *parent, BVH *sharedBVH);
	BVH *bvh;
	
	void flatViewEnable();

	public slots:
        // called by the timer in the main window
	void updateTime();


	// animation controls
	void playpauseUpdate();
	void jointModeUpdate();
	void updateHighlight(int dir);

	signals:
		void jKeyPressed();
		void pKeyPressed();
		void arrowKeyPressed(int dir);

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	private:


	void cube(bool shadow=false);
	void floor();

	void polygon(int, int, int, int);

	double _time;

	bool playingActive;
	bool jointModeActive;
	bool flatViewEnabled;

	}; // class GLPolygonWidget
	
#endif
