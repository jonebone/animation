// to run on mac: use OpenGl/glu.h
#include <OpenGl/glu.h>
// to run on Dec10: use GL/glu.h
//#include <GL/glu.h>
#include <QGLWidget>
#include "FluidSimWidget.h"

static const float PI = 3.1416f; // rounded since PI is 3.14159...


// constructor
FluidSimWidget::FluidSimWidget(QWidget *parent):
  QGLWidget(parent),
  _time(0.0),
  _stepDelay(1.0),
  _isPlaying(false),
  _densityKernel(2),
  _pressureKernel(3),
  _viscKernel(1),
  _includePressure(true), 
  _includeGravity(true), 
  _includeVisc(false), 
  _includeSurfaceT(false)
	{ // constructor
    particles = std::vector<fluidParticle>();

    // build grid
    // 5/0.2 = 25
    // 15/0.2 = 75
    grid.reserve(26 * 76);
    for (int i = 0; i < 26 * 76; i ++) {
      grid[i] = std::vector<int>();
    }
} // constructor


void FluidSimWidget::placeInGrid(int particleIndex) { // placeInGrid(int particleIndex)
	int row, col;
  col = (int) ((particles[particleIndex].position.x + 2.5) * 5 ); // cast to int truncates
  row = (int) ((particles[particleIndex].position.y) * 5 );
  if (col < 0 || col > 25) std::cerr << col << " <- " << row << " " << particleIndex << std::endl;
  if (row < 0 || row > 76) std::cerr << col << " -> " << row << " " << particleIndex << std::endl;
  grid[col * 51 + row].push_back(particleIndex);
} // placeInGrid(int particleIndex)

std::vector<fluidParticle> FluidSimWidget::gridNeighbors(int particleIndex) {
  int row, col;
  col = (int) ((particles[particleIndex].position.x + 2.5) * 5 ); // cast to int truncates
  row = (int) ((particles[particleIndex].position.y) * 5 );

  std::vector<fluidParticle> ret = std::vector<fluidParticle>();

  for (int i = -1; i <= 1; i++) {
    if (col + i < 0 || col + i > 25) continue;
    for (int j = -1; j <= 1; j++) {
      if (row + j < 0 || row + j > 75) continue;
      for (auto i : grid[(col + i) * 51 + row + j])
        ret.push_back(particles[i]);
    }
  }
  return ret;
}


// called when OpenGL context is set up
void FluidSimWidget::initializeGL() { // initializeGL()
	// set the widget background colour
	glClearColor(0.4, 0.4, 0.4, 0.0);
} // initializeGL()


// called every time the widget is resized
void FluidSimWidget::resizeGL(int w, int h) { // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

        
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-5.0, 5.0, -1.0, 9.0, -4.0, 4.0);

} // resizeGL()


//////////////////////////////////////////////
/////////////////// SLOTS ////////////////////
//////////////////////////////////////////////

void FluidSimWidget::updateTime(){
  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
    this->repaint();
  }
}	

void FluidSimWidget::updateStepDelay(int val){
  _stepDelay = (float) val;
  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
    this->repaint();
  }
}	

void FluidSimWidget::updatePlayPause(){
  _isPlaying = !_isPlaying;
  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
    this->repaint();
  }
}	

void FluidSimWidget::resetSim(){
  simInit();
  _time = 0.0;

  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
  }

  this->repaint();
}	

void FluidSimWidget::updateDensityKernel(int useKernel){
  _densityKernel = useKernel;
  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
    this->repaint();
  }
}	

void FluidSimWidget::updatePressureKernel(int useKernel){
  _pressureKernel = useKernel;
  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
    this->repaint();
  }
}	

void FluidSimWidget::updateViscKernel(int useKernel){
  _viscKernel = useKernel;
  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
    this->repaint();
  }
}	



void FluidSimWidget::updateIncludePressure(bool inc){
  _includePressure = !_includePressure;
  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
    this->repaint();
  }
}	// updateIncludePressure()

void FluidSimWidget::updateIncludeGravity(bool inc){
  _includeGravity = !_includeGravity;
  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
    this->repaint();
  }
}	// updateIncludeGravity()

void FluidSimWidget::updateIncludeVisc(bool inc){
  _includeVisc = !_includeVisc;
  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
    this->repaint();
  }
}	// updateIncludeVisc()

void FluidSimWidget::updateIncludeSurfaceT(bool inc){
  _includeSurfaceT = !_includeSurfaceT;
  if (_isPlaying) {
    _time += 1.0 / _stepDelay;
    for (int i = 0; i < 15; i ++)
      leapFrogStep(timestep / (float)_stepDelay);
    this->repaint();
  }
}	// updateIncludeSurfaceT()

//////////////////////////////////////////////
/////////////////// PAINT ////////////////////
//////////////////////////////////////////////

void FluidSimWidget::floor() {
  // single thick line to represent floor
  
  GLfloat floor = -0.02;
	
  glColor3f(0.f, 0.f, 0.f);
  glLineWidth(5.0f);

  glNormal3f(0.,1.,0.);
  glBegin(GL_LINES);
    glVertex3f( -10.0,  floor,  0.0);
    glVertex3f(  10.0,  floor,  0.0);
  glEnd();

} // floor()

void FluidSimWidget::bucket(){
  // draws in the bucket
  glColor3f(0.8f, 0.5f, 0.3f);
  glLineWidth(6.0f);

  glBegin(GL_LINES);
    glVertex3f( -2.5,  5.0,  0.0);
    glVertex3f( -2.5,  0.0,  0.0);
    glVertex3f( -2.5,  0.0,  0.0);
    glVertex3f(  2.5,  0.0,  0.0);
    glVertex3f(  2.5,  0.0,  0.0);
    glVertex3f(  2.5,  5.0,  0.0);
  glEnd();

} // bucket()

void FluidSimWidget::renderParticles(){
  // gaurd against uninitialized things
  if (particles.size() == 0) {
    this->simInit();
  }

  //glColor3f(0.1f, 0.2f, 0.8f);
  glPointSize(5.0f);

  glBegin(GL_POINTS);
    for (auto f : particles) {
      //glColor3d(0.1, 0.2, 0.1 * exp((double)f.density - 1.0));
      glColor3f(0.0, 0.0, maxDensity / f.density);
      glVertex3f(f.position.x, f.position.y, 0.0);
    }
  glEnd();

} // bucket()




// called every time the widget needs painting
void FluidSimWidget::paintGL() { // paintGL()
	// clear the widget
  glEnable(GL_DEPTH_TEST);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


  glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

  this->renderParticles();

  this->bucket();
  this->floor();

	
	// flush to screen
	glFlush();	

} // paintGL()

//////////////////////////////////////////////
///////////////// KERNELS ////////////////////
//////////////////////////////////////////////

// all gradients have been solved for by me 

// poly6 kernel from lectures

float FluidSimWidget::poly6(float r) {
  // return 0 if r > h or r < 0
  if (0 > r || r > kernel_h) return 0.f;

  float h9 = (float) pow(kernel_h, 9.0);
  return ( 315.0 / ( 64.0 * PI * h9 ) ) * (float) pow(h2  - r * r , 3);
} // poly6

float FluidSimWidget::gradPoly6(float r) {
  // since the only actual variable in poly6 is r, which is scalar distance, the gradiant of poly6 is the first derivative wrt r
  // poly6' = -r * 945/(32 π h^9) * (h^2 - r^2)^2

  if (0 > r || r > kernel_h) return 0.f; // return 0 if r > h or r < 0

  float h9 = (float) pow(kernel_h, 9.0);
  return -r * ( 945.0 / ( 32.0 * PI * h9 ) ) * (float) pow(h2  - r * r , 2);
} // gradPoly6

// spiky kernel from lectures

float FluidSimWidget::spiky(float r) {
  // return 0 if r > h or r < 0
  if (0 > r || r > kernel_h) return 0.f;

  float h6 = h2 * h2 * h2; // h^6 = (h^2)^3
  return ( 15.0 / (PI * h6 ) ) * (float) pow(kernel_h  - r , 3);
} // spiky

float FluidSimWidget::gradSpiky(float r) {
  if (0 > r || r > kernel_h) return 0.f; // return 0 if r > h or r < 0

  float h6 = h2 * h2 * h2; // h^6 = (h^2)^3
  return ( -45.0 / (PI * h6 ) ) * (float) pow(kernel_h  - r , 2);
} // gradSpiky

float FluidSimWidget::viscLaplacian(float r) {
  if (0 > r || r > kernel_h) return 0.f; // return 0 if r > h or r < 0

  float h6 = h2 * h2 * h2; // h^6 = (h^2)^3
  return ( 45.0 / (PI * h6 ) ) * (kernel_h  - r);
} // viscLaplacian()


// 2D poly6, spiky, and viscosity kernels as given by the paper "SPH Based Shallow Water Simulation" by P. Bucher et. al., 2011
// 2D poly6: 4 / (π h^8) { (h^2 - r^2)^3 if 0 ≤ r ≤ h; 0 otherwise

float FluidSimWidget::poly6_2D(float r) {
  // return 0 if r > h or r < 0
  if (0 > r || r > kernel_h) return 0.f;

  float h8 = (float) pow(kernel_h, 8.0);
  return ( 4.0 / ( PI * h8 ) ) * (float) pow(h2  - r * r , 3);
} // poly6_2D

float FluidSimWidget::gradPoly6_2D(float r) {
  // poly6_2D' = -r * 24/(π h^8) * (h^2 - r^2)^2

  if (0 > r || r > kernel_h) return 0.f; // return 0 if r > h or r < 0

  float h8 = (float) pow(kernel_h, 8.0);
  return -r * ( 24.0 / ( PI * h8 ) ) * (float) pow(h2  - r * r , 2);
} // gradPoly6_2D


float FluidSimWidget::spiky_2D(float r) {
  // return 0 if r > h or r < 0
  if (0 > r || r > kernel_h) return 0.f;

  float h5 = h2 * h2 * kernel_h; // h^5 = (h^2)^2 * h
  return ( 10.0 / (PI * h5 ) ) * (float) pow(kernel_h  - r , 3);
} // spiky_2D

float FluidSimWidget::gradSpiky_2D(float r) {
  if (0 > r || r > kernel_h) return 0.f; // return 0 if r > h or r < 0

  float h5 = h2 * h2 * kernel_h; // h^5 = (h^2)^2 * h
  return ( -30.0 / (PI * h5 ) ) * (float) pow(kernel_h  - r , 2);
} // gradSpiky_2D

float FluidSimWidget::viscLaplacian_2D(float r) {
  if (0 > r || r > kernel_h) return 0.f; // return 0 if r > h or r < 0

  float h5 = h2 * h2 * kernel_h; // h^6 = (h^2)^3
  return ( 40.0 / (PI * h5 ) ) * (kernel_h  - r);
} // viscLaplacian_2D()


// ambiguous kernel call so the kernel to be used for each part can be selected
float FluidSimWidget::kernel(float r, int useKernel) {
  // return 0 case is the same for all kernels
  if (0 > r || r > kernel_h) return 0.f;

  switch (useKernel) {
    case 0:
      return poly6(r);
    case 1:
      return spiky(r);
    // 2D kernels
    case 2:
      return poly6_2D(r);
    case 3:
      return spiky_2D(r);
    default: // should never be encountered
      std::cerr << "\n ! non-existant kernel selected !\n";
  }
  return 0.f; // just in case
} // kernel()

float FluidSimWidget::gradKernel(float r, int useKernel) {
  // return 0 case is the same for all kernels
  if (0 > r || r > kernel_h) return 0.f;

  switch (useKernel) {
    case 0:
      return gradPoly6(r);
    case 1:
      return gradSpiky(r);
    // 2D kernels
    case 2:
      return gradPoly6_2D(r);
    case 3:
      return gradSpiky_2D(r);
    default: // should never be encountered
      std::cerr << "\n ! non-existant kernel selected !\n";
  }
  return 0.f; // just in case
} // gradKernel()

float FluidSimWidget::viscKernel(float r, int useKernel) {
  // return 0 case is the same for all kernels
  if (0 > r || r > kernel_h) return 0.f;

  switch (useKernel) {
    case 0:
      return viscLaplacian(r);
    // 2D kernels
    case 1:
      return viscLaplacian_2D(r);
    default: // should never be encountered
      std::cerr << "\n ! non-existant kernel selected !\n";
  }
  return 0.f; // just in case
} // viscKernel()



//////////////////////////////////////////////
///////////////////  SIM  ////////////////////
//////////////////////////////////////////////


void FluidSimWidget::simInit() {
  // starting box goes from (xPos,yPos) to (xMax, yMax)
  float xPos = -2.f;
  float yPos =  4.f;
  float xMax = xPos + 2.0f;
  float yMax = yPos + 3.0f;
  //float xMax = xPos + 1.0f;
  //float yMax = yPos + 1.0f;

  particles.clear();

  float step = kernel_h / 2;
  //float step = 0.2;

  float variance = 0.f;

  // populate vector
  for (xPos = -2.0f; xPos < xMax; xPos += step)
    for (yPos = 4.0f; yPos < yMax; yPos += step) {
      variance = kernel_h/4 - ((float)(rand() % 100) )/ 1000.0;

      // make new particle
      fluidParticle f;
      f.position = Cartesian3(xPos + variance, yPos, 0.f);
      f.force = Cartesian3();
      f.velocity = Cartesian3();

      // add particle to vector
      particles.push_back(f);
      // add particle to grid
      placeInGrid(particles.size() - 1);
    }
  // end loops

  // calc densities
  calcDensities();
} // simInit()


void FluidSimWidget::calcDensities() {
  maxDensity = 0.1f;
  Cartesian3 Ri;
  float newDensity = 0;

  for (int i = 0; i < (int)particles.size(); i++) {
    Ri = particles[i].position;
    particles[i].density = 0.f;
    newDensity = 0;
    //for (auto j : particles) { // counts self in density calculation
    //for (auto j : particles) if (!(j.position == Ri)) { // does not count self in density calculation
    //for (auto j : gridNeighbors(i)) if (!(j.position == Ri)) { // does not count self in density calculation
    for (auto j : gridNeighbors(i)) { // count self in density calculation
      newDensity += particleMass * kernel((Ri - j.position).length(), _densityKernel);
    }
    //if (newDensity == 0.f) newDensity = particleMass * kernel(0.f , _densityKernel); // count self if density is 0
    particles[i].density = newDensity;
    if (newDensity > maxDensity) maxDensity = newDensity;
  }

}// calcDensities()

void FluidSimWidget::addForces() {
  // make sure densities are up to date first!
  calcDensities();

  Cartesian3 i2j, pressureForce, viscosityForce;
  float r, iPressure, jPressure;

  // iterate through all particles the three forces to each one
  for (int i = 0; i < (int)particles.size(); i++) {

    // add gravity (just -9.8 * density in the y direction)
    if (_includeGravity)
      particles[i].force = particles[i].force + Cartesian3(0.f, -9.8f * particles[i].density, 0.f);
    //

    // including either pressure or viscosity requires a loop through other particles
    if (_includePressure || _includeVisc) {
      pressureForce  = Cartesian3();
      viscosityForce = Cartesian3();
      iPressure = gas_k * (particles[i].density - restDensity);
      //for (auto j : particles) if (!(j.position == particles[i].position)) { // if statement skips self 
      for (auto j : gridNeighbors(i)) if (!(j.position == particles[i].position)) { // if statement skips self 
        i2j = j.position - particles[i].position;
        r = i2j.length();
        if (r <= kernel_h && r >= 0.f) {
          // pressure calculations
          if (_includePressure) {
            // symmetric form requires each one
            jPressure = gas_k * (j.density - restDensity);
            pressureForce = pressureForce + i2j.unit() * particleMass * ( ( iPressure + jPressure ) / ( 2.f * j.density) ) * gradKernel(r, _pressureKernel);
          }

          // viscosity calculations
          if (_includeVisc) {
            viscosityForce = viscosityForce + ( (j.velocity - particles[i].velocity) / j.density) * particleMass * viscKernel(r, _viscKernel);
          }

          
        }
      } // end pressure/viscosity loop

      // add pressure and viscosity forces to particle forces
      particles[i].force = particles[i].force + pressureForce + viscosityForce * visc_mu;
    }
    

  }
}// addForces()

void FluidSimWidget::leapFrogStep(float deltaT) {

  // remake grid to be filled after positions change
  grid.reserve(26 * 76);
  for (int i = 0; i < 26 * 51; i ++)
    grid[i] = std::vector<int>();

  for (int i = 0; i < (int)particles.size(); i++) {
    // first half update of velocity
    particles[i].velocity = particles[i].velocity + (particles[i].force / particles[i].density ) * (deltaT / 2.f);
    // reset forces to be updated later
    particles[i].force = Cartesian3();
    // update x
    particles[i].position = particles[i].position + particles[i].velocity * deltaT;
    // bounds checking needed w changing positions
    checkBounds(i);
    // then add back to grid!
    placeInGrid(i);

  } // end first half loop

  // only recalculate forces after all positions have been updated
  addForces();

  for (int i = 0; i < (int)particles.size(); i++) {
    // second half update of velocity
    particles[i].velocity = particles[i].velocity + (particles[i].force / particles[i].density ) * (deltaT / 2.f);
  }

  // clear grid at end of step
  grid.clear();


}// leapFrogStep()

void FluidSimWidget::checkBounds(int particleIndex) {
  // checks if particle at index is out of the bucket, if so, fixes it

  // if above bucket, ignore :)
  //if (particles[particleIndex].position.y > 5.0) return;

  // x bounds
  if (particles[particleIndex].position.x > 2.5) {
    particles[particleIndex].position.x = 2.5 - (particles[particleIndex].position.x - 2.5) * bucketRestitution;
    particles[particleIndex].velocity.x *= -bucketRestitution;
  } else if (particles[particleIndex].position.x < -2.5) {
    particles[particleIndex].position.x = -2.5 + (-2.5 - particles[particleIndex].position.x) * bucketRestitution;
    particles[particleIndex].velocity.x *= -bucketRestitution;
  }

  // y bound
  if (particles[particleIndex].position.y < 0.0) {
    particles[particleIndex].position.y *= -bucketRestitution;
    particles[particleIndex].velocity.y *= -bucketRestitution;
  }

  // y upper bound for the safety of my grid
  if (particles[particleIndex].position.y > 15.0) {
    particles[particleIndex].position.y = 15.0 - (particles[particleIndex].position.y - 15.0) * bucketRestitution;
    particles[particleIndex].velocity.y *= -bucketRestitution;
  }
  
} // checkBounds()