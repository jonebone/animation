#include "FluidSimWindow.h"

// constructor / destructor
FluidSimWindow::FluidSimWindow(QWidget *parent)
	: QWidget(parent)
	{ // constructor

	// create menu bar
	menuBar = new QMenuBar(this);
	
	// create file menu
	fileMenu = menuBar->addMenu("&File");

	// create the action
	actionQuit = new QAction("&Quit", this);

	// leave signals & slots to the controller
	
	// add the item to the menu
	fileMenu->addAction(actionQuit);
	
	// create the window layout
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

	// create other layouts
	forcesLayout = new QBoxLayout(QBoxLayout::LeftToRight);
	menuLayout = new QBoxLayout(QBoxLayout::LeftToRight);
	labelsLayout = new QBoxLayout(QBoxLayout::TopToBottom);
	interactivesLayout = new QBoxLayout(QBoxLayout::TopToBottom);

	// create main widget
	simWidget = new FluidSimWidget(this);
	windowLayout->addWidget(simWidget, 10);


	// add forces layout to window
	windowLayout->addLayout(forcesLayout);

	// add label to forces layout
	forcesLayout->addWidget(new QLabel("forces enabled: "));

	// create force check boxes
	includePressureCheck = new QCheckBox("pressure ");
	includePressureCheck->setChecked(true);
	includeGravityCheck =  new QCheckBox("gravity  ");
	includeGravityCheck-> setChecked(true);
	includeViscCheck =     new QCheckBox("viscosity");
	includeViscCheck->    setChecked(false);
	includeSurfaceTCheck = new QCheckBox("surface tension");
	includeSurfaceTCheck->setChecked(false);

	// add force boxes to force layout
	forcesLayout->addWidget(includePressureCheck);
	forcesLayout->addWidget(includeGravityCheck);
	forcesLayout->addWidget(includeViscCheck);
	forcesLayout->addWidget(includeSurfaceTCheck);


	// add menu layout to window
	windowLayout->addLayout(menuLayout);

	// add sub layouts to menu layout
	menuLayout->addLayout(labelsLayout);
	menuLayout->addLayout(interactivesLayout, 1);

	// create slider
	nVerticesSlider = new QSlider(Qt::Horizontal);
	nVerticesSlider->setMinimum(1);
	nVerticesSlider->setMaximum(10);
	interactivesLayout->addWidget(nVerticesSlider);
	// slider label
	labelsLayout->addWidget(new QLabel("timestep (fast <-> slow)"));

	// create selectors
	QStringList kernels = {"poly6", "spiky", "2D poly6", "2D spiky"};
	QStringList viscKernels = {"viscosity", "2D viscosity"};
	densityKernelSelection = new QComboBox();
	densityKernelSelection->addItems(kernels);
	densityKernelSelection->setCurrentIndex(2);
	pressureKernelSelection = new QComboBox();
	pressureKernelSelection->addItems(kernels);
	pressureKernelSelection->setCurrentIndex(3);
	viscKernelSelection = new QComboBox();
	viscKernelSelection->addItems(viscKernels);
	viscKernelSelection->setCurrentIndex(1);

	// add to layout
	interactivesLayout->addWidget(densityKernelSelection);
	interactivesLayout->addWidget(pressureKernelSelection);
	interactivesLayout->addWidget(viscKernelSelection);
	// add labels
	labelsLayout->addWidget(new QLabel("density   kernel:"));
	labelsLayout->addWidget(new QLabel("pressure  kernel:"));
	labelsLayout->addWidget(new QLabel("viscosity kernel:"));

	ptimer = new QTimer(this);
	ptimer->start(20);

	connect(ptimer, SIGNAL(timeout()),  simWidget, SLOT(updateTime()));
	connect(nVerticesSlider, SIGNAL(valueChanged(int)),  simWidget, SLOT(updateStepDelay(int)));
	// key press signals and slots
	connect(simWidget, SIGNAL(enterKeyPressed()),  simWidget, SLOT(updatePlayPause()));
	connect(simWidget, SIGNAL(rKeyPressed()),  simWidget, SLOT(resetSim()));
	// selectors signals and slots
	connect(densityKernelSelection,  SIGNAL(currentIndexChanged(int)), simWidget, SLOT( updateDensityKernel(int)  ));
	connect(pressureKernelSelection, SIGNAL(currentIndexChanged(int)), simWidget, SLOT( updatePressureKernel(int) ));
	connect(viscKernelSelection, 	 SIGNAL(currentIndexChanged(int)), simWidget, SLOT( updateViscKernel(int)     ));
	// check boxes signals and slots
	connect(includePressureCheck, SIGNAL(toggled(bool)), simWidget, SLOT(updateIncludePressure(bool) ));
	connect(includeGravityCheck,  SIGNAL(toggled(bool)), simWidget, SLOT(updateIncludeGravity(bool)  ));
	connect(includeViscCheck,     SIGNAL(toggled(bool)), simWidget, SLOT(updateIncludeVisc(bool)     ));
	connect(includeSurfaceTCheck, SIGNAL(toggled(bool)), simWidget, SLOT(updateIncludeSurfaceT(bool) ));

 

} // constructor

FluidSimWindow::~FluidSimWindow() { // destructor
	delete ptimer;
	delete nVerticesSlider;
	delete simWidget;
	delete windowLayout;
	delete actionQuit;
	delete fileMenu;
	delete menuBar;
} // destructor

// resets all the interface elements
void FluidSimWindow::ResetInterface() { // ResetInterface()
	nVerticesSlider->setMinimum(1);
	nVerticesSlider->setMaximum(10);

	//don't use the slider for now

	//	nVerticesSlider->setValue(thePolygon->nVertices);
	
	// now force refresh
	simWidget->update();
	update();
} // ResetInterface()

void FluidSimWindow::keyPressEvent(QKeyEvent *event) {
	if (event->key() == Qt::Key_Return) {
        //std::cout << "return key was pressed\n";
		emit simWidget->enterKeyPressed();
	}
    else if (event->key() == Qt::Key_R) emit simWidget->rKeyPressed();

}