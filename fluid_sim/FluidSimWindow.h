#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QComboBox>
#include <QStringList>
#include <QCheckBox>
#include <QTimer>
#include <QBoxLayout>
#include <QtWidgets>
#include "FluidSimWidget.h"

class FluidSimWindow: public QWidget
	{ 
	public:
       
	
	// constructor / destructor
	FluidSimWindow(QWidget *parent);
	~FluidSimWindow();

	// visual hierarchy
	// menu bar
	QMenuBar *menuBar;
		// file menu
		QMenu *fileMenu;
			// quit action
			QAction *actionQuit;

	// window layout
	QBoxLayout *windowLayout;
	// forces bar layout
	QBoxLayout *forcesLayout;
	// menu layout
	QBoxLayout *menuLayout;
	// sub menu layouts
	QBoxLayout *labelsLayout;
	QBoxLayout *interactivesLayout;

	// beneath that, the main widget
	FluidSimWidget *simWidget;
	// and a slider for the timestep adjustment
	QSlider *nVerticesSlider;

	// combo boxes for selecting kernels for different parts
	QComboBox *densityKernelSelection;
	QComboBox *pressureKernelSelection;
	QComboBox *viscKernelSelection;

	// force inclusion checkboxes
	QCheckBox *includePressureCheck;
	QCheckBox *includeGravityCheck;
	QCheckBox *includeViscCheck;
	QCheckBox *includeSurfaceTCheck;
	

	// a timer
	QTimer *ptimer;

	// resets all the interface elements
	void ResetInterface();

	// key detection
	void keyPressEvent(QKeyEvent *event);
}; 
	
#endif
