#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>

// debugging includes
#include <iostream>

// fluid sim includes
#include <vector>
#include <cmath>
#include "Cartesian3.h" // from comp5812M Foundations of Modelling & Rendering

// fluid particle struct
typedef struct fluidParticle {
	float density;
	Cartesian3 position;
	Cartesian3 velocity;
	Cartesian3 force;
} fluidParticle;

class FluidSimWidget: public QGLWidget { // 
	// not split into model-view-controller due to time management constraints

	Q_OBJECT

	public:
	FluidSimWidget(QWidget *parent);

	// SIGNALS
	public: signals:
	void enterKeyPressed();
	void rKeyPressed();

	// SLOTS
	public slots:
        // called by the timer in the main window
	void updateTime();
		// called by slider
	void updateStepDelay(int val);
		// called by keys
	void updatePlayPause();
	void resetSim();
		// called by kernel selectors
	void updateDensityKernel(int useKernel);
	void updatePressureKernel(int useKernel);
	void updateViscKernel(int useKernel);
		// called by force checkboxes
	void updateIncludePressure(bool inc);
	void updateIncludeGravity(bool inc);
	void updateIncludeVisc(bool inc);
	void updateIncludeSurfaceT(bool inc);

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

////// PRIVATE ///////
	private:


  /// properties
	// interface properties
	double _time, _stepDelay;
	bool _isPlaying;
	int _densityKernel, _pressureKernel, _viscKernel;
	bool _includePressure, _includeGravity, _includeVisc, _includeSurfaceT;
	

	// particle things
	std::vector<fluidParticle> particles;
	std::vector<std::vector<int>> grid; 

	void placeInGrid(int particleIndex);
	void removeFromGrid(int particleIndex);
	std::vector<fluidParticle> gridNeighbors(int particleIndex);

	// sim things
	float timestep = 0.0007f;

/*
	// universal particle properties
	static constexpr float particleMass = 100.75;
	//float gas_k = 1.7;
	//float restDensity = 1.1;
	//float kernel_h = 01.4f;
	//float kernel_h = 2.88f;
	static constexpr float kernel_h = 2.08f;
	float h2 = kernel_h * kernel_h;
	float gas_k = 12.f; //27
	// mu of water is 1000 centipoise at 20C, 1 Pascal/sec = 10 Poise
	// according to https://www.pipeflow.com/pipe-pressure-drop-calculations/fluid-viscosity
	float visc_mu = 10.1;
	float restDensity = 2.35;
	float bucketRestitution = 0.5;
*/

	// universal particle properties
	// decent vals :)
	static constexpr float particleMass = 2.0417;
	static constexpr float kernel_h = 0.2f;
	float h2 = kernel_h * kernel_h;
	//float gas_k = 2430.f;
	float gas_k = 20.f;
	float visc_mu = 197.27;
	float restDensity = 209.995;
	float bucketRestitution = 0.005;


	
  /// methods
	// kernels
	// unspecified kernel, useKernel specifies which to use
	float kernel(float r, int useKernel = 0);
	float gradKernel(float r, int useKernel = 0);
	float viscKernel(float r, int useKernel = 0);

	float poly6(float r);
	float gradPoly6(float r);
	
	float spiky(float r);
	float gradSpiky(float r);

	float poly6_2D(float r);
	float gradPoly6_2D(float r);
	
	float spiky_2D(float r);
	float gradSpiky_2D(float r);

	// only implement the Laplacian of the viscosity kernel bc we only ever use the Laplacian of it
	float viscLaplacian(float r);
	float viscLaplacian_2D(float r);

	// sim stuff
	void simInit();
	void calcDensities();
	void addForces();
	void updatePositions(float deltaT);
	void simStep(float deltaT);
	void leapFrogStep(float deltaT);
	void checkBounds(int particleIndex);



	// fluid sim render parts
	float maxDensity;
	void bucket();
	void floor();
	void renderParticles();

	

}; // class GLPolygonWidget
	
#endif
