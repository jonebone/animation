# README for code distributed for use in COMP 5821M 2021-22
#
# modified for animation 

To compile, execute the following on feng-linux:
module add qt/5.13.0
qmake -project QT+=opengl
qmake
make

--------------------------------------

To compile on OSX:
Use Homebrew to install qt

qmake -project QT+=opengl
qmake
make
--------------------------------------