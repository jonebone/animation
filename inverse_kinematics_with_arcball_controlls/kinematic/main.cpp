//////////////////////////////////////////////////////////////////////
//
//  University of Leeds
//  COMP 5812M Foundations of Modelling & Rendering
//  User Interface for Coursework
//
//  September, 2020
//
//  -----------------------------
//  main.cpp
//  -----------------------------
//  
//  Loads assets, then passes them to the render window. This is very far
//  from the only way of doing it.
//  
////////////////////////////////////////////////////////////////////////

// system libraries
#include <iostream>
#include <fstream>

// QT
#include <QApplication>

// local includes
#include "RenderWindow.h"
#include "BVH.h"
#include "RenderParameters.h"
#include "RenderController.h"

// main routine
int main(int argc, char **argv)
    { // main()
    // initialize QT
    QApplication renderApp(argc, argv);

    // check the args to make sure there's an input file
    if (argc != 2 && argc != 3) 
        { // bad arg count
        // print an error message
        std::cout << "Please run with input file path as arg1 and optional arg2 as save path" << std::endl; 
        // and leave
        return 0;
        } // bad arg count

    //  use the argument to create a height field &c.
    BVH bvh;

    if (argc == 3) bvh.saveFileName = string(argv[2]);

    // open the input files for the geometry & texture
    std::ifstream bvhSourceFile(argv[1]);

    // try reading it
    if (!(bvhSourceFile.good())) { // bad file passed 
        std::cout << "File provided was bad " << argv[1] << std::endl;
        return 0;
    } // bad file passed
    // try loading it
    bvh.Load(argv[1]);
    if(!bvh.IsLoadSuccess()) { // could not load file
        std::cout << "Failed to load " << argv[1] << std::endl;
        return 0;
    } // could not load file


    // create some default render parameters
    RenderParameters renderParameters;

    // use the object & parameters to create a window
    RenderWindow renderWindow(&bvh, &renderParameters, argv[1]);

    // create a controller for the window
    RenderController renderController(&bvh, &renderParameters, &renderWindow);

    //  set the initial size
    renderWindow.resize(723, 580);

    // show the window
    renderWindow.show();

    // set QT running
    return renderApp.exec();

    } // main()
