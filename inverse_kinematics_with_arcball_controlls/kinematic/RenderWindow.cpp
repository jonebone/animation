/////////////////////////////////////////////////////////////////
//
//  University of Leeds
//  COMP 5812M Foundations of Modelling & Rendering
//  User Interface for Coursework
//
//  September, 2020
//
//  -----------------------------
//  Render Window
//  -----------------------------
//  
//  The render window class is really just a container
//  for tracking the visual hierarchy.  While it is
//  entirely possible to use Qt Creator, I try to avoid
//  over-commitment to it because I need to write code in
//  multiple environments, some of which are not well-suited
//  to IDEs in general, let alone Qt Creator
//
//  Also, by providing sample code, the didactic purpose of 
//  showing how things fit together is better served.
//
/////////////////////////////////////////////////////////////////

#include "RenderWindow.h"
#include "RenderParameters.h"

// constructor
RenderWindow::RenderWindow
        (
        // the object to be rendered
        BVH         *newBVH, 
        // the model object storing render parameters
        RenderParameters        *newRenderParameters,
        // the title for the window (with default value)
        const char              *windowName
        )
    // call the inherited constructor
    // NULL indicates that this widget has no parent
    // i.e. that it is a top-level window
    :
    // member instantiation
    QWidget(NULL),
    bvh(newBVH),
    renderParameters(newRenderParameters)
    { // RenderWindow::RenderWindow()
    // set the window's title
    setWindowTitle(QString(windowName));
    
    // initialise the grid layout
    windowLayout = new QGridLayout(this);
    
    // create all of the widgets, starting with the custom render widgets
    renderWidget                = new RenderWidget              (newBVH,     		newRenderParameters,        this);

    // construct custom arcball Widgets
    modelRotator                = new ArcBallWidget             (                       this);

    // construct standard QT widgets
    
    // spatial sliders
    xTranslateSlider            = new QSlider                   (Qt::Horizontal,        this);
    yTranslateSlider            = new QSlider                   (Qt::Vertical,          this);
    zoomSlider                  = new QSlider                   (Qt::Vertical,          this);
    // set it invisible bc it breaks if we remove it entirely
    zoomSlider->setVisible(false);

    lambdaSlider                = new QSlider                   (Qt::Vertical,          this);
    // progress slider
    progressSlider              = new QSlider                   (Qt::Horizontal,        this);
    
    // labels for sliders and arcballs
    modelRotatorLabel           = new QLabel                    ("I apologize for this UI", this);
    yTranslateLabel             = new QLabel                    ("Y",                       this);
    zoomLabel                   = new QLabel                    ("Lambda",                  this);

    // changing label
    jointLabel                  = new QLabel                    ("press [j]",            this);
    controlLabel = new QLabel("Control Joints:\n [c] to add\n [r] to reset", this);

    // make timer
    _timer = new QTimer(this);
    _timer->start(20);


    
    // add all of the widgets to the grid               Row         Column      Row Span    Column Span
    
    int nStacked = 6;
    
    windowLayout->addWidget(renderWidget,               0,          1,          nStacked,   1           );
    windowLayout->addWidget(yTranslateSlider,           0,          2,          nStacked,   1           );
    windowLayout->addWidget(lambdaSlider,               0,          3,          nStacked,   1           );

    // the stack on the end

    windowLayout->addWidget(modelRotator,               0,          4,          1,          1           );
    windowLayout->addWidget(modelRotatorLabel,          5,          4,          1,          1           );
    windowLayout->addWidget(jointLabel,                 3,          4,          1,          1           );
    windowLayout->addWidget(controlLabel,               1,          4,          1,          1);

    // Translate Slider Row
    windowLayout->addWidget(xTranslateSlider,           nStacked,   1,          1,          1           );
    windowLayout->addWidget(yTranslateLabel,            nStacked,   2,          1,          1           );
    
    // progress slider row
    windowLayout->addWidget(progressSlider,             nStacked + 1,1,          1,          1           );
    windowLayout->addWidget(new QLabel("progress", this),            nStacked + 1,2,          1,          1           );

    // nothing in column 3
    windowLayout->addWidget(zoomLabel,                  nStacked,   3,          1,          1           );
    // add spacers
    windowLayout->addItem(new QSpacerItem(540, 10),     nStacked,   1,          1,          1           );
    windowLayout->addItem(new QSpacerItem(400, 10),     nStacked,   4,          1,          1           );
    
    
    // now reset all of the control elements to match the render parameters passed in
    ResetInterface();
} // RenderWindow::RenderWindow()

// routine to reset interface
// sets every visual control to match the model
// gets called by the controller after each change in the model
void RenderWindow::ResetInterface()
    { // RenderWindow::ResetInterface()
   
    // set sliders
    // x & y translate are scaled to notional unit sphere in render widgets
    // but because the slider is defined as integer, we multiply by a 100 for all sliders
    xTranslateSlider        ->setMinimum        ((int) (TRANSLATE_MIN                               * PARAMETER_SCALING));
    xTranslateSlider        ->setMaximum        ((int) (TRANSLATE_MAX                               * PARAMETER_SCALING));
    xTranslateSlider        ->setValue          ((int) (renderParameters -> xTranslate              * PARAMETER_SCALING));
    
    yTranslateSlider        ->setMinimum        ((int) (TRANSLATE_MIN                               * PARAMETER_SCALING));
    yTranslateSlider        ->setMaximum        ((int) (TRANSLATE_MAX                               * PARAMETER_SCALING));
    yTranslateSlider        ->setValue          ((int) (renderParameters -> yTranslate              * PARAMETER_SCALING));

    // set up Progress slider
    progressSlider          ->setMinimum        (0);
    progressSlider          ->setMaximum        (bvh->num_frame);

    // zoom slider is a logarithmic scale, so we want a narrow range
    lambdaSlider              ->setMinimum        (0);
    lambdaSlider              ->setMaximum        (200);
    lambdaSlider              ->setValue          (100);


    // now flag them all for update 
    renderWidget            ->update();
    modelRotator            ->update();
    xTranslateSlider        ->update();
    yTranslateSlider        ->update();
    zoomSlider              ->update();
} // RenderWindow::ResetInterface()

void RenderWindow::keyPressEvent(QKeyEvent *event) {
	if (event->key() == Qt::Key_J) {
		emit renderWidget->jKeyPressed();
	}
	else if (event->key() == Qt::Key_P) {
		emit renderWidget->pKeyPressed();
	}

    else if (event->key() == Qt::Key_K) emit renderWidget->kKeyPressed();
    else if (event->key() == Qt::Key_C) emit renderWidget->cKeyPressed();
    else if (event->key() == Qt::Key_R) emit renderWidget->rKeyPressed();
    else if (event->key() == Qt::Key_G) emit renderWidget->gKeyPressed();
	// arrow keys
	else if (event->key() == Qt::Key_Left)  emit renderWidget->arrowKeyPressed( 1);
	else if (event->key() == Qt::Key_Right) emit renderWidget->arrowKeyPressed( 2);
	else if (event->key() == Qt::Key_Up)    emit renderWidget->arrowKeyPressed(-1);
	else if (event->key() == Qt::Key_Down)  emit renderWidget->arrowKeyPressed( 0);

    // gain keys
    else if (event->key() == Qt::Key_BracketLeft)    emit renderWidget->changeGainKeyPressed(-0.1);
    else if (event->key() == Qt::Key_BracketRight)   emit renderWidget->changeGainKeyPressed( 0.1);

    // point moving 
    else if (event->key() == Qt::Key_A)   emit renderWidget->pointMover(-1, 0, 0);
    else if (event->key() == Qt::Key_D)   emit renderWidget->pointMover( 1, 0, 0);

    else if (event->key() == Qt::Key_W)   emit renderWidget->pointMover( 0, 1, 0);
    else if (event->key() == Qt::Key_S)   emit renderWidget->pointMover( 0,-1, 0);

    else if (event->key() == Qt::Key_Q)   emit renderWidget->pointMover( 0, 0, 1);
    else if (event->key() == Qt::Key_E)   emit renderWidget->pointMover( 0, 0,-1);
}

