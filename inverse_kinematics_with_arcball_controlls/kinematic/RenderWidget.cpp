//////////////////////////////////////////////////////////////////////
//
//  University of Leeds
//  COMP 5812M Foundations of Modelling & Rendering
//  User Interface for Coursework
//
//  September, 2020
//
//  -----------------------------
//  Render Widget
//  -----------------------------
//  
//  Since the render code is in the geometric object class
//  this widget primarily sets up the transformation matrices and 
//  lighting.
//
//  It implements the UI for an arcball controller, but in an
//  abstractable way that allows it to share an arcball with another
//  widget (the arcball controller) - thus you can manipulate the
//  object either directly in the widget or indirectly through the
//  arcball displayed visually.
//  
//  It also supports translation by visual dragging.  This results in
//  needing three mouse buttons, and not all systems have them, so it
//  will be up to the user to make sure that they can use it on their
//  own machine (especially on OSX)
//
//  Since the controls are (potentially) shared with other widgets, 
//  this widget is only responsible for scaling the x,y of mouse events
//  then passing them to the controller
//  
////////////////////////////////////////////////////////////////////////

#include <math.h>


// include the header file
#include "RenderWidget.h"

// constructor
RenderWidget::RenderWidget
        (   
        // the geometric object to show
        BVH 	*newBVH,
        // the render parameters to use
        RenderParameters    *newRenderParameters,
        // parent widget in visual hierarchy
        QWidget             *parent
        )
    // the : indicates variable instantiation rather than arbitrary code
    // it is considered good style to use it where possible
    : 
    // start by calling inherited constructor with parent widget's pointer
    QOpenGLWidget(parent),
    // then store the pointers that were passed in
    bvh(newBVH),
    _time(0.0),
    _lambda(1.0),
    playingActive(true),
    jointSelectionActive(false),
    renderParameters(newRenderParameters)
    { // constructor
    // leaves nothing to put into the constructor body
} // constructor    

// destructor
RenderWidget::~RenderWidget() { // destructor
    // empty (for now)
    // all of our pointers are to data owned by another class
    // so we have no responsibility for destruction
    // and OpenGL cleanup is taken care of by Qt
} // destructor                                                                 


// update label text method
void RenderWidget::updateBVHText(){
    QString jointChildren = QString("");
    if (jointSelectionActive) {
        // use -> as bulletpoints
        // use =>> to indicate highlighted joint
        // add parent if it exists
        if (bvh->selectedJoint->parent != NULL) {
            if (bvh->selectedJoint->parent == bvh->highlightedJoint) jointChildren.append("[^] to set parent as selected\n");
            else jointChildren.append("[^] to highlight parent\n");
            jointChildren.append("\nSelected Joint Parent:\n");
            if (bvh->selectedJoint->parent == bvh->highlightedJoint) jointChildren.append("=>> ");
            else jointChildren.append("-> ");
            jointChildren.append(bvh->selectedJoint->parent->name.c_str());
            if (bvh->selectedJoint->parent == bvh->highlightedJoint) jointChildren.append("\n\n[down] to highlight selected");
            else if (bvh->selectedJoint == bvh->highlightedJoint) jointChildren.append("\n\n[^] to highlight parent");
        }
        // display selected joint
        jointChildren.append("\n\nSelected Joint:\n");
        if (bvh->selectedJoint == bvh->highlightedJoint) {
            jointChildren.append("=>> ");
            jointChildren.append(bvh->selectedJoint->name.c_str());
            jointChildren.append("\n\n[down] to highlight children");
        }
        else {
            jointChildren.append("-> ");
            jointChildren.append(bvh->selectedJoint->name.c_str());
            jointChildren.append("\n\n[^] to highlight selected");
        }
        
        // display children in list
        jointChildren.append("\n\nSelected Joint Children:");
        for (int i = 0; i < bvh->selectedJoint->children.size(); i++) {
            if (bvh->selectedJoint->children[i] == bvh->highlightedJoint) jointChildren.append("\n=>> ");
            else jointChildren.append("\n-> ");
            jointChildren.append(bvh->selectedJoint->children[i]->name.c_str());
        }
        jointChildren.append("\n\n[<] and [>] navigate children \n[down] to set highlighted joint as selected");
    } else { // if joint selection mode is off, set label accordingly
        jointChildren.append("Press [j] to begin joint manipulation mode");
    }

    emit this->highlightedJointChanged(jointChildren);
}

/// *********************************************************************** ///
/// **************************  SLOTS  ************************************ ///
/// *********************************************************************** ///


void RenderWidget::updateTime(){
    if (playingActive) {
        _time += 1.0;
        emit this->frameProgress((int)_time % bvh->num_frame);
    }
    this->repaint();
}	

void RenderWidget::updateTime(int time){
    _time = time;
    if (playingActive) _time += 1.0;
    this->repaint();
}	

void RenderWidget::updateLambda(int lambda){
    _lambda = lambda / 100;
    if (playingActive) _time += 1.0;
    this->repaint();
}	

void RenderWidget::playPauseUpdate(){
    playingActive = !playingActive;
    if (playingActive) _time += 1.0;
    this->repaint();
}

void RenderWidget::jointModeUpdate(){
    if (jointSelectionActive) {
        jointSelectionActive = false;
        bvh->joint_highlight_enabled = false;
        //std::cout << bvh->getWorldPosition() << std::endl;
        bvh->applyIK();
    } else {
        jointSelectionActive = true;
        bvh->joint_highlight_enabled = true;
        bvh->initJointMode();
    }
    updateBVHText();
    if (playingActive) _time += 1.0;
    this->repaint();
}

void RenderWidget::highlightUpdate(int dir){
  if (jointSelectionActive) {
    bvh->shiftHighlight(dir);
    updateBVHText();
  }
  if (playingActive) _time += 1.0;
  this->repaint();
}

void RenderWidget::addGainToJoint(double gain) {
    if (!jointSelectionActive || !(bvh->controlMode)) return;
    bvh->highlightedJoint->weight += gain;
    if (bvh->controlMode) {
        QString weightStr = QString("Weights:\n");
        for (int i = 0; i < bvh->joints.size(); i++) {
            if (bvh->joints[i]->weight != 1.0) {
                weightStr.append(" ");
                weightStr.append(std::to_string(bvh->joints[i]->weight).c_str());
                weightStr.append(" ");
                weightStr.append(bvh->joints[i]->name.c_str());
                weightStr.append("\n");
            }
        }
        emit this->jointWeightsChanged(weightStr);
    }
}

void RenderWidget::updateControlMode() {
    bvh->controlMode = !(bvh->controlMode);
    if (bvh->controlMode) {
        QString weightStr = QString("Weights:\n");
        for (int i = 0; i < bvh->joints.size(); i++) {
            if (bvh->joints[i]->weight != 1.0) {
                weightStr.append(" ");
                weightStr.append(std::to_string(bvh->joints[i]->weight).c_str());
                weightStr.append(" ");
                weightStr.append(bvh->joints[i]->name.c_str());
                weightStr.append("\n");
            }
        }
        emit this->jointWeightsChanged(weightStr);
    }
}

void RenderWidget::pointMoved(int xd, int yd, int zd) {
    if (!jointSelectionActive) return;
    bvh->highlightedJoint->mod[0] += xd * 1.0;
    bvh->highlightedJoint->mod[1] += yd * 1.0;
    bvh->highlightedJoint->mod[2] += zd * 1.0;
    if (playingActive) _time += 1.0;
    this->repaint();
}

void RenderWidget::applyIK() {
    bvh->applyIK((int)_time % bvh->num_frame, _lambda);
    this->repaint();
}

void RenderWidget::addControl() {
    bvh->controlJoints.push_back(bvh->highlightedJoint);
    QString controlStr = QString("Control Joints:\n ");
    for (int i = 0; i < bvh->controlJoints.size(); i++) {
        controlStr.append(bvh->controlJoints[i]->name.c_str());
        controlStr.append("\n ");
    }
    controlStr.append("\n [c] to add\n [r] to reset\n");
    emit this->controlJointsChanged(controlStr);
}

void RenderWidget::clearControl() {
    bvh->controlJoints.clear();
}



/// *********************************************************************** ///
/// **************************  OPENGL  *********************************** ///
/// *********************************************************************** ///


// called when OpenGL context is set up
void RenderWidget::initializeGL() { // RenderWidget::initializeGL()
    // set lighting parameters (may be reset later)
    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
    
    // background is grey
    glClearColor(0.3, 0.3, 0.3, 1.0);

    // enable depth-buffering
	glEnable(GL_DEPTH_TEST);
    
    // sometimes this widget can misbehave until resized
    // so force resize
    resizeGL(1,1);
} // RenderWidget::initializeGL()

// called every time the widget is resized
void RenderWidget::resizeGL(int w, int h) { // RenderWidget::resizeGL()
    // reset the viewport
    glViewport(0, 0, w, h);
    
    // set projection matrix to be glOrtho based on zoom & window size
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    // compute the aspect ratio of the widget
    float aspectRatio = (float) w / (float) h;
    
    // we want to capture a sphere of radius 1.0 without distortion
    // so we set the ortho projection based on whether the window is portrait (> 1.0) or landscape
    // portrait ratio is wider, so make bottom & top -1.0 & 1.0
    if (aspectRatio > 1.0)
        glOrtho(-aspectRatio * 4.0, aspectRatio * 4.0, -4.0, 4.0, -4.0, 4.0);
    // otherwise, make left & right -1.0 & 1.0
    else
        glOrtho(-4.0, 4.0, -4.0/aspectRatio, 4.0/aspectRatio, -4.0, 4.0);

} // RenderWidget::resizeGL()
    
// called every time the widget needs painting
void RenderWidget::paintGL() { // RenderWidget::paintGL()
    // clear the buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set model view matrix based on stored translation, rotation &c.
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
        
    // start with lighting turned off
    glDisable(GL_LIGHTING);

	// translate by the visual translation
	glTranslatef(renderParameters->xTranslate, renderParameters->yTranslate, 0.0f);

	// apply rotation matrix from arcball
	glMultMatrixf(renderParameters->rotationMatrix.columnMajor().coordinates);

    // tell the object to draw itself, 
    // passing in the render parameters for reference
    bvh->RenderFigure((int)_time % bvh->num_frame, 0.15f);
    // draw axis on highlighted joint via getting the real world position
    // in order to confirm that real world position is right
    if (jointSelectionActive && false) {
        glPushMatrix();
            //glLoadIdentity();
            Homogeneous4 t = bvh->getWorldPosition((int)_time % bvh->num_frame, false);

            double *data = bvh->motion + ((int)_time % bvh->num_frame) * bvh->num_channel;
            Matrix4 tm = bvh->recursiveGetWorldPosition(bvh->highlightedJoint, data, true);
            glScalef(0.15, 0.15, 0.15);
            
            glTranslatef(-data[0], -data[1], -data[2]);
            glTranslatef(t[0], t[1], t[2]);
            //glMultMatrixf(tm.columnMajor().coordinates);
            bvh->RenderAxes();

        glPopMatrix();
    }

} // RenderWidget::paintGL()
    



/// *********************************************************************** ///
/// ***************************  MOUSE  *********************************** ///
/// *********************************************************************** ///

// mouse-handling
void RenderWidget::mousePressEvent(QMouseEvent *event)
    { // RenderWidget::mousePressEvent()
    // store the button for future reference
    int whichButton = event->button();
    // scale the event to the nominal unit sphere in the widget:
    // find the minimum of height & width   
    float size = (width() > height()) ? height() : width();
    // scale both coordinates from that
    float x = (2.0 * event->x() - size) / size;
    float y = (size - 2.0 * event->y() ) / size;
    
    // and we want to force mouse buttons to allow shift-click to be the same as right-click
    int modifiers = event->modifiers();
    
    // shift-click (any) counts as right click
    if (modifiers & Qt::ShiftModifier)
        whichButton = Qt::RightButton;
    
    // send signal to the controller for detailed processing
    emit BeginScaledDrag(whichButton, x,y);
    } // RenderWidget::mousePressEvent()
    
void RenderWidget::mouseMoveEvent(QMouseEvent *event)
    { // RenderWidget::mouseMoveEvent()
    // scale the event to the nominal unit sphere in the widget:
    // find the minimum of height & width   
    float size = (width() > height()) ? height() : width();
    // scale both coordinates from that
    float x = (2.0 * event->x() - size) / size;
    float y = (size - 2.0 * event->y() ) / size;
    
    // send signal to the controller for detailed processing
    emit ContinueScaledDrag(x,y);
    } // RenderWidget::mouseMoveEvent()
    
void RenderWidget::mouseReleaseEvent(QMouseEvent *event)
    { // RenderWidget::mouseReleaseEvent()
    // scale the event to the nominal unit sphere in the widget:
    // find the minimum of height & width   
    float size = (width() > height()) ? height() : width();
    // scale both coordinates from that
    float x = (2.0 * event->x() - size) / size;
    float y = (size - 2.0 * event->y() ) / size;
    
    // send signal to the controller for detailed processing
    emit EndScaledDrag(x,y);
    } // RenderWidget::mouseReleaseEvent()
