#include <fstream>
#include <string>
#include <iostream>
#include <OpenGL/glu.h>

#include "BVH.h"
// NOTE: This might not be needed
// #include "stdafx.h"



// Contractor
BVH::BVH() {
  motion = NULL;
  Clear();
  joint_highlight_enabled = false;
  highlightedJoint = selectedJoint = NULL;
}

// Contractor
BVH::BVH(const char *bvh_file_name) {
  motion = NULL;
  Clear();

  Load(bvh_file_name);
}

// Destructor
BVH::~BVH() { 
  if (saveFileName != "") Save(saveFileName.c_str());
  Clear(); 
}

// Clear all information
void BVH::Clear() {
  unsigned int i;
/*
  // print channels just to check
  string enumTypeStrings[6] = {
    "X_ROTATION",
    "Y_ROTATION",
    "Z_ROTATION",
    "X_POSITION",
    "Y_POSITION",
    "Z_POSITION"
  };
  for (i = 0; i < joints.size(); i++) {
    std::cout << joints[i]->name << std::endl;
    for (int j = 0; j < joints[i]->channels.size(); j++) {
      std::cout << "  Channel " << j << " index "<< joints[i]->channels[j]->index << " type " << enumTypeStrings[joints[i]->channels[j]->type] << std::endl;
    }
  }
*/
  for (i = 0; i < channels.size(); i++) delete channels[i];
  for (i = 0; i < joints.size(); i++) delete joints[i];
  if (motion != NULL) delete motion;
  
  is_load_success = false;

  file_name = "";
  motion_name = "";

  num_channel = 0;
  channels.clear();
  joints.clear();
  joint_index.clear();

  num_frame = 0;
  interval = 0.0;
  motion = NULL;
}

//
// Load BVH file
//
void BVH::Load(const char *bvh_file_name) {
#define BUFFER_LENGTH 1024 * 4

  ifstream file;
  char line[BUFFER_LENGTH];
  char *token;
  char separater[] = " :,\t\r\n";
  vector<Joint *> joint_stack;
  Joint *joint = NULL;
  Joint *new_joint = NULL;
  bool is_site = false;
  double x, y, z;
  int i, j;

  // Initialize
  Clear();

  // Setting file information (file name / operation name)
  file_name = bvh_file_name;
  const char *mn_first = bvh_file_name;
  const char *mn_last = bvh_file_name + strlen(bvh_file_name);
  if (strrchr(bvh_file_name, '\\') != NULL)
    mn_first = strrchr(bvh_file_name, '\\') + 1;
  else if (strrchr(bvh_file_name, '/') != NULL)
    mn_first = strrchr(bvh_file_name, '/') + 1;
  if (strrchr(bvh_file_name, '.') != NULL)
    mn_last = strrchr(bvh_file_name, '.');
  if (mn_last < mn_first) mn_last = bvh_file_name + strlen(bvh_file_name);
  motion_name.assign(mn_first, mn_last);


  // Open file
  file.open(bvh_file_name, ios::in);
  if (file.is_open() == 0) return;  // Exit if the file cannot be opened

  // Read hierarchical information
  while (!file.eof()) {
    // Abnormal termination when the end of the file is reached
    if (file.eof()) goto bvh_error;

    // Read one line and get the first word
    file.getline(line, BUFFER_LENGTH);
    token = strtok(line, separater);

    // If there is a blank line, go to the next line
    if (token == NULL) continue;

    // Start of joint block
    if (strcmp(token, "{") == 0) {
      // Stack the current joint
      joint_stack.push_back(joint);
      joint = new_joint;
      continue;
    }
    // End of joint block
    if (strcmp(token, "}") == 0) {
      // Remove the current joint from the stack
      joint = joint_stack.back();
      joint_stack.pop_back();
      is_site = false;
      continue;
    }

    // Start of joint information
    if ((strcmp(token, "ROOT") == 0) || (strcmp(token, "JOINT") == 0)) {
      // Create joint data
      new_joint = new Joint();
      new_joint->index = joints.size();
      new_joint->parent = joint;
      new_joint->has_site = false;
      new_joint->offset[0] = 0.0;
      new_joint->offset[1] = 0.0;
      new_joint->offset[2] = 0.0;
      new_joint->site[0] = 0.0;
      new_joint->site[1] = 0.0;
      new_joint->site[2] = 0.0;
      joints.push_back(new_joint);
      if (joint) joint->children.push_back(new_joint);
      
      // Read joint name
      token = strtok(NULL, "");
      while (*token == ' ') token++;
      new_joint->name = token;

      // Add to index
      joint_index[new_joint->name] = new_joint;
      continue;
    }

    // Start of end information
    if ((strcmp(token, "End") == 0)) {
      new_joint = joint;
      is_site = true;
      continue;
    }

    // Joint offset or end position information
    if (strcmp(token, "OFFSET") == 0) {
      // Read the coordinate values
      token = strtok(NULL, separater);
      x = token ? atof(token) : 0.0;
      token = strtok(NULL, separater);
      y = token ? atof(token) : 0.0;
      token = strtok(NULL, separater);
      z = token ? atof(token) : 0.0;
      // Set the coordinate value for the joint offset
      if (is_site) {
        joint->has_site = true;
        joint->site[0] = x;
        joint->site[1] = y;
        joint->site[2] = z;
      } else
      // Set the coordinate value at the end position
      {
        joint->offset[0] = x;
        joint->offset[1] = y;
        joint->offset[2] = z;
        // add initial mod values
        joint->mod[0] = joint->mod[1] = joint->mod[2] = 0.0;
        // add weight
        joint->weight = 1.0;
      }
      continue;
    }

    // Joint channel information
    if (strcmp(token, "CHANNELS") == 0) {
      // Read the number of channels
      token = strtok(NULL, separater);
      joint->channels.resize(token ? atoi(token) : 0);

      // Read channel information
      for (i = 0; i < joint->channels.size(); i++) {
        // Create channel
        Channel *channel = new Channel();
        channel->joint = joint;
        channel->index = channels.size();
        channels.push_back(channel);
        joint->channels[i] = channel;

        // Judgment of channel type
        token = strtok(NULL, separater);
        if (strcmp(token, "Xrotation") == 0)
          channel->type = X_ROTATION;
        else if (strcmp(token, "Yrotation") == 0)
          channel->type = Y_ROTATION;
        else if (strcmp(token, "Zrotation") == 0)
          channel->type = Z_ROTATION;
        else if (strcmp(token, "Xposition") == 0)
          channel->type = X_POSITION;
        else if (strcmp(token, "Yposition") == 0)
          channel->type = Y_POSITION;
        else if (strcmp(token, "Zposition") == 0)
          channel->type = Z_POSITION;
      }
    }

    // Move to the Motion data section
    
    if (strcmp(token, "MOTION") == 0) break;
  }

  // Load motion information
  file.getline(line, BUFFER_LENGTH);
  token = strtok(line, separater);
  if (strcmp(token, "Frames") != 0) goto bvh_error;
  token = strtok(NULL, separater);
  if (token == NULL) goto bvh_error;
  num_frame = atoi(token);

  file.getline(line, BUFFER_LENGTH);
  token = strtok(line, ":");
  if (strcmp(token, "Frame Time") != 0) goto bvh_error;
  token = strtok(NULL, separater);
  if (token == NULL) goto bvh_error;
  interval = atof(token);

  num_channel = channels.size();
  motion = new double[num_frame * num_channel];

  // Load motion data
  for (i = 0; i < num_frame; i++) {
    file.getline(line, BUFFER_LENGTH);
    token = strtok(line, separater);
    for (j = 0; j < num_channel; j++) {
      if (token == NULL) goto bvh_error;
      motion[i * num_channel + j] = atof(token);
      token = strtok(NULL, separater);
    }
  }

  // Close file
  file.close();

  // Successful loading
  is_load_success = true;

  gainArray.resize(num_channel);
  controlMode = false;
  return;

bvh_error:
  file.close();
}

void BVH::Save(const char *bvh_file_name) {
  ofstream file;
  file.open(bvh_file_name, ios::trunc);
  if (file.is_open() == 0) {
    std::cerr << "uh oh fucky wucky\n";
    return;
  }

  // print top
  file << "HIERARCHY\n";
  
  // print root
  Joint *j = joints[0];
  file << "ROOT " << j->name << "\n{\n";
  string tabs = "\t";
  file << tabs << "OFFSET " << j->offset[0] << " " << j->offset[1] << " " << j->offset[2] << "\n";
  file << tabs << "CHANNELS " << j->channels.size();
  for (int i = 0; i < j->channels.size(); i ++) 
    file << " " << enumTypeStrings[j->channels[i]->type];
  file << "\n";
  
  // recursively call children
  for (int i = 0; i < j->children.size(); i++)
    recursiveWriteChildren(file, j->children[i], 1);
  file << "}\n";
  // print motion to file
  file << "MOTION\n";
  file << "Frames: " << num_frame << "\n";
  file << "Frame Time: " << interval << "\n";
  for (int i = 0; i < num_frame; i++) {
    for (int j = 0; j < num_channel; j++)
      file << (motion + i * num_channel)[j] << " ";
    file << "\n";
  }

  file.close();
}

void BVH::recursiveWriteChildren(ofstream &file, Joint *j, int depth) {
  string tabs = "\t";
  for (int i = 1; i < depth; i++) tabs.append("\t");

  // write name
  file << tabs << "JOINT " << j->name << "\n";
  file << tabs << "{\n";

  // need another tab snuck in here 
  file << tabs << "\tOFFSET " << j->offset[0] << " " << j->offset[1] << " " << j->offset[2] << "\n";
  file << tabs << "\tCHANNELS " << j->channels.size();
  for (int i = 0; i < j->channels.size(); i ++) 
    file << " " << enumTypeStrings[j->channels[i]->type];
  file << "\n";

  // print site if relevent
  if (j->has_site) {
    file << tabs << "\tEnd Site\n";
    file << tabs << "\t{\n";
    file << tabs << "\t\tOFFSET " << j->site[0] << " " << j->site[1] << " " << j->site[2] << "\n";
    file << tabs << "\t}\n";
  }

  // recursively print children
  for (int i = 0; i < j->children.size(); i++)
    recursiveWriteChildren(file, j->children[i], depth + 1);


  file << tabs << "}\n";
  

}

//
// BVH skeleton / posture drawing function
//

#include <math.h>

#include "glut/glut.h"

void BVH::initJointMode(){
  if (highlightedJoint == NULL || selectedJoint == NULL) {
    highlightedJoint = selectedJoint = joints[0];
    //std::cout << selectedJoint->name << std::endl;
  }
}

void BVH::shiftHighlight(int dir) {
  // just in case things have not been initialized yet
  if (highlightedJoint == NULL || selectedJoint == NULL) {
    highlightedJoint = selectedJoint = joints[0];
  }
  // method to move which is the highlighted joint
  if (dir > 0) { // left and right (laterally in tree)
    // if joint or parent is highlighted, can't move laterally
    // so highlighted joint must be a child of selected joint; selected joint must be parent of highlighted joint
    if (highlightedJoint->parent == selectedJoint) {
      // need to have more than one child to be able to move laterally
      int numChildren = selectedJoint->children.size();
      for (int i = 0; i < numChildren; i++) { // luckily for implicitly handles this check
        if (highlightedJoint == selectedJoint->children[i]) {
          int newIndex = (i + ((dir == 1) ? -1 : 1));
          //handle -1 and numChildren cases, which would otherwise be out of bounds
          if (newIndex < 0) newIndex = numChildren - 1;
          else if (newIndex == numChildren) newIndex = 0;
          highlightedJoint = selectedJoint->children[newIndex];
          return;
        }
      } // end for 
    } // end is highlighted joint a child of selected check
  } // end lateral movement
  else if (dir == 0) { // move down tree
    if (highlightedJoint == selectedJoint && selectedJoint->children.size() > 0) {
      highlightedJoint = selectedJoint->children[0];
    }
    else if (highlightedJoint->parent == selectedJoint) {
      selectedJoint = highlightedJoint;
    }
    else if (highlightedJoint == selectedJoint->parent) {
      highlightedJoint = selectedJoint;
    }
  }
  else if (dir == -1) { // move up tree
    if (highlightedJoint == selectedJoint && selectedJoint->parent != NULL) {
      highlightedJoint = selectedJoint->parent;
    }
    else if (highlightedJoint->parent == selectedJoint) {
      highlightedJoint = selectedJoint;
    }
    else if (highlightedJoint == selectedJoint->parent) {
      selectedJoint = highlightedJoint;
    }
  }
}


void BVH::getHOffset(double *offset) {
  Joint *j = highlightedJoint;
  offset[0] += j->offset[0];
  offset[1] += j->offset[1];
  offset[2] += j->offset[2];
  while (j->parent != NULL) {
    j = j->parent;
    offset[0] += j->offset[0];
    offset[1] += j->offset[1];
    offset[2] += j->offset[2];
  }
}

void BVH::getModOffset(double *offset) {
  Joint *j = highlightedJoint;
  offset[0] += j->offset[0] + j->mod[0];
  offset[1] += j->offset[1] + j->mod[1];
  offset[2] += j->offset[2] + j->mod[2];
  while (j->parent != NULL) {
    j = j->parent;
    offset[0] += j->offset[0] + j->mod[0];
    offset[1] += j->offset[1] + j->mod[1];
    offset[2] += j->offset[2] + j->mod[2];
  }
}

double BVH::distOfOffset(double *offset) {
  return sqrt(offset[0] * offset[0] + offset[1] * offset[1] + offset[2] * offset[2]);
}

double BVH::distWMod(double *offset, double *mod) {
  double combined[3] = {offset[0] + mod[0], offset[1] + mod[1], offset[2] + mod[2]};
  return sqrt(combined[0] * combined[0] + combined[1] * combined[1] + combined[2] * combined[2]);
}

double BVH::getMaxDistFromRoot() {
  Joint *j = highlightedJoint;
  double dist = distOfOffset(&(j->offset[0]));
  while (j->parent != NULL) {
    j = j->parent;
    dist += distOfOffset(&(j->offset[0]));
  }
  return dist;
}

Matrix4 BVH::recursiveGetWorldPosition(Joint *j, double *data, bool includeMod) {
  /// this method is designed to start from a child node and recurse its way up the tree 
  // from some joint, it returns LHS * transformations
  // where transformations are the offset translation and joint angle rotation transformations of that joint
  // and LHS is all its parents transformations

  /// init matrices
  Matrix4 m = Matrix4();
  Matrix4 parentM = Matrix4();
  Matrix4 transformation = Matrix4();
  m.SetIdentity();
  parentM.SetIdentity();
  
  // if joint is root, translate by channels 0,1,2 aka data[0] data[1] data[2]
  // no need to recurse in this case; no parent to call the recursive function on
  if (j->parent == NULL) transformation.SetTranslation(Cartesian3(data[0], data[1], data[2])); 
  else { 
    // otherwise get left hand side of return recursively
    parentM = recursiveGetWorldPosition(j->parent, data, includeMod);
    // and the transformation is done via offset rather than channel
    transformation.SetTranslation(Cartesian3(j->offset[0], j->offset[1], j->offset[2]));
  }
  // apply whichever translation was set
  m = m * transformation;

  // set transformation matrix to be identity in case it is not set to any rotation
  transformation.SetIdentity();
  
  if (includeMod) {
    for (int i = 0; i < j->channels.size(); i++) {
      Channel *channel = j->channels[i];
      if (channel->type == X_ROTATION)
        transformation.SetRotation(Cartesian3(1.0f, 0.0f, 0.0f), (j->mod[0] + data[channel->index]) * M_PI / 180.0f);
      else if (channel->type == Y_ROTATION)
        transformation.SetRotation(Cartesian3(0.0f, 1.0f, 0.0f), (j->mod[1] + data[channel->index]) * M_PI / 180.0f);
      else if (channel->type == Z_ROTATION)
        transformation.SetRotation(Cartesian3(0.0f, 0.0f, 1.0f), (j->mod[2] + data[channel->index]) * M_PI / 180.0f);
      m = m * transformation;
      transformation.SetIdentity();
    }
  }
  else {
    for (int i = 0; i < j->channels.size(); i++) {
      Channel *channel = j->channels[i];
      if (channel->type == X_ROTATION)
        transformation.SetRotation(Cartesian3(1.0f, 0.0f, 0.0f), (data[channel->index] * M_PI / 180.0f));
      else if (channel->type == Y_ROTATION)
        transformation.SetRotation(Cartesian3(0.0f, 1.0f, 0.0f), (data[channel->index] * M_PI / 180.0f));
      else if (channel->type == Z_ROTATION)
        transformation.SetRotation(Cartesian3(0.0f, 0.0f, 1.0f), (data[channel->index] * M_PI / 180.0f));
      m = m * transformation;
      transformation.SetIdentity();
    }
  }

  
  return parentM * m;
}

Homogeneous4 BVH::getWorldPosition(int frame, bool includeMod){
  // offset motion pointer by frame * number of channels so that it now points to the channel values at that frame 
  Matrix4 transform = recursiveGetWorldPosition(highlightedJoint, motion + frame * num_channel, includeMod);
  //std::cout << transform << std::endl;
  return transform * Homogeneous4(0.0, 0.0, 0.0, 1.0);
}

Matrix4 BVH::getDeltaP(Joint *theta, int frame){
  /// This method should return a 4x4 matrix of 
  // row 1: Position p1 with no change to joint angles
  // row 2: Position p2 with X joint rotation incremented by delta theta
  // row 3: Position p2 with Y joint rotation incremented by delta theta
  // row 4: Position p2 with Z joint rotation incremented by delta theta
  double deltaTheta = 0.001;

  // offset motion pointer by frame * number of channels so that it now points to the channel values at that frame 
  double *data = motion + frame * num_channel;

  // init values to be used throughout
  Matrix4 ret = Matrix4();
  Homogeneous4 zeroPoint = Homogeneous4(0.0, 0.0, 0.0, 1.0);
  Homogeneous4 row;
  // get row 1 
  Matrix4 transform = recursiveGetWorldPosition(highlightedJoint, data, false);
  Homogeneous4 row1 = transform * zeroPoint;
  // put row 1 in matrix
  for (int i = 0; i < 4; i++)
    ret[0][i] = row1[i];
  
  /// do each rotation
  // init ChannelEnum array
  ChannelEnum typeArray[3] = {
    X_ROTATION,
    Y_ROTATION,
    Z_ROTATION
  };
  for (int i = 1; i < 4; i++) { // loop for rows 1,2,3 of matrix
    // loop through joint channels
    for (int c = 0; c < theta->channels.size(); c++) {
      Channel *channel = theta->channels[c];
      // if channel is the appropriate one for the row
      if (channel->type == typeArray[i - 1]){
        // save initial joint angle value in order to reset later
        double initialValue = data[channel->index];
        // change joint angle by delta Theta
        data[channel->index] += deltaTheta;
        // get new transformation matrix and then new row value
        transform = recursiveGetWorldPosition(highlightedJoint, data, false);
        row = (transform * zeroPoint);
        // put new row into matrix
        for (int j = 0; j < 4; j++) ret[i][j] = row[j];
        
        // reset joint angle
        data[channel->index] = initialValue;
      } // end channel == type if
      
    } // end joint channels loop
  } // end row loop
  
  return ret;
}

double BVH::getModDistFromRoot() {
  Joint *j = highlightedJoint;
  double offset[3] = {0.0};
  getModOffset(&offset[0]);
  double dist = distOfOffset(&offset[0]);
  return distOfOffset(&offset[0]);
}

void BVH::populateGainArray() {
  for (int i = 0; i < joints.size(); i++) {
    Joint *joint = joints[i];
    for (int j = 0; j < joint->channels.size(); j ++)
      gainArray[joint->channels[j]->index] = joint->weight;
  }
  std::cout << "populated\n";
}

// takes array of current positions, array of target positions, and a tolerance (defaults to 0.5)
// for use in a while loop, so want to return true if we're outside the tolerance on any one of our points
bool BVH::multiControlTest(Homogeneous4 *currs, Homogeneous4 *targets, float tol) {
  for (int i = 0; i < controlJoints.size(); i++) {
    //std::cout << (targets[i] - currs[i]).Vector().length() << std::endl;
    if ((targets[i] - currs[i]).Vector().length() >= tol) return true;
  }
  return false;
}

bool BVH::isControl(Joint *j) {
  for (int i = 0; i < controlJoints.size(); i++)
    if (controlJoints[i] == j) return true;
  return false;
}

void BVH::applyIK(int frame, double lambda) {
  // define deltaTheta
  double deltaTheta = 0.1;
  //double deltaTheta = 90.0;

  // if no control points, add hilighted point as only control point
  if (controlJoints.size() == 0) controlJoints.push_back(highlightedJoint); 
  int numControlJoints = controlJoints.size();

  if (controlMode) populateGainArray();
  //std::cout << "populated" <<std::endl;
  

  // array of current positions and targets
  Homogeneous4 currentPositions[numControlJoints];
  Homogeneous4 targetPositions[numControlJoints];
  // fill these arrays
  Homogeneous4 zeroPoint = Homogeneous4(0,0,0,1);
  for (int i = 0; i < numControlJoints; i++) {
    // get current position and target position for each control joint
    currentPositions[i] = recursiveGetWorldPosition(controlJoints[i], motion + frame * num_channel, false) * zeroPoint;
    targetPositions[i]  = recursiveGetWorldPosition(controlJoints[i], motion + frame * num_channel, true ) * zeroPoint;
  }

  // while outside the tolerance, keep going :)
  while (multiControlTest(&currentPositions[0], &targetPositions[0])) {
    // make target - current matrix
    Eigen::MatrixXd ptpc(3 * numControlJoints, 1);
    for (int i = 0; i < numControlJoints; i++) {
      ptpc(0 + i * 3,0) = targetPositions[i][0] - currentPositions[i][0];
      ptpc(1 + i * 3,0) = targetPositions[i][1] - currentPositions[i][1];
      ptpc(2 + i * 3,0) = targetPositions[i][2] - currentPositions[i][2];
    }

    /// make Jacobian
    Homogeneous4 p2Array[numControlJoints];
    //  (3 * numControlJoints) x num_channel-3 matrix bc num_channel is the number of joint angles + the root's X_Position, Y_Position, and Z_Position.
    Eigen::MatrixXd jacMatrix(3 * numControlJoints, num_channel - 3);
    Eigen::MatrixXd thetaMatrix(num_channel - 3, 1);
    // offset by 3 to skip position channels of root
    double *data = motion + 3 + frame * num_channel;


    // for each channel, calculate the corresponding jacobian column
    for (int i = 0; i < num_channel - 3; i++) {
      // put channel into an Eigen matrix for easy math later
      thetaMatrix(i,0) = data[i];
      // change by deltaTheta
      data[i] += deltaTheta;
      // calculate p2 for all control joints, using currentPositions as p1
      for (int j = 0; j < numControlJoints; j++) {
        p2Array[j] = recursiveGetWorldPosition(controlJoints[j], motion + frame * num_channel, false) * zeroPoint;
        // put (p2-pc)/deltaTheta in the right place in the jacobian
        for(int k = 0; k < 3; k++) jacMatrix(k + j * 3, i) = (p2Array[j][k]-currentPositions[j][k]) / deltaTheta;
      }
      // change back so it doesn't mess up later things
      data[i] -= deltaTheta;
  
    }

    // create dampening matrix
    Eigen::MatrixXd dampeningMatrix(3 * numControlJoints, 3 * numControlJoints);
    dampeningMatrix.setIdentity();
    dampeningMatrix *= lambda * lambda;

    /// compute pseudoinverse
    // include dampening
    Eigen::MatrixXd invJ;
    //invJ = jacMatrix.transpose() * (jacMatrix * jacMatrix.transpose()).inverse(); // no dampening version
    invJ = jacMatrix.transpose() * (jacMatrix * jacMatrix.transpose() + dampeningMatrix).inverse();
    // do math
    Eigen::MatrixXd theta2(num_channel - 3, 1);
    theta2 = thetaMatrix + invJ*ptpc;

    // if we are doing control 
    if (controlMode) {
      Eigen::MatrixXd zVec(num_channel - 3, 1);
      for (int i = 0; i < num_channel - 3; i++) 
        zVec(i, 0) = gainArray[i + 3] * pow(data[i] - theta2(i, 0), 2.0);
      Eigen::MatrixXd idMatrix(num_channel - 3, num_channel - 3);
      idMatrix.setIdentity();
      theta2 = thetaMatrix + (invJ * jacMatrix - idMatrix) * zVec;
    }
    


    // write new joint angles to movement
    for (int i = 0; i < num_channel - 3; i++) {
      data[i] = theta2(i, 0);
    }

    // update current positions
    for (int i = 0; i < numControlJoints; i++)
      currentPositions[i] = recursiveGetWorldPosition(controlJoints[i], motion + frame * num_channel, false) * zeroPoint;
  }
  
  // clear mod
  for (int i = 0; i < joints.size(); i ++) {
    joints[i]->mod[0] = 0.0;
    joints[i]->mod[1] = 0.0;
    joints[i]->mod[2] = 0.0;
  }
}


// draw axes 
void BVH::RenderAxes() {
  // set the lines to be obvious in width
  glLineWidth(4.0);
  // now draw one line for each axis in different colours
  glBegin(GL_LINES);
    // X axis is red
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(1.0, 0.0, 0.0);
    // Y axis is green
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 1.0, 0.0);
    // Z axis is red
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 1.0);
    
    
    // draw floor lines
    glLineWidth(1.0);
    for(float i = 0.0f; i < 20.0f; i += 1.0f) {
      // X axis is red
      glColor3f(0.5, 0.0, 0.0);
      glVertex3f(-10.0, -0.01, 10.0 - i);
      glVertex3f( 10.0, -0.01, 10.0 - i);
      // Z axis is red
      glColor3f(0.0, 0.0, 0.5);
      glVertex3f(10.0 - i, -0.01, -10.0);
      glVertex3f(10.0 - i, -0.01,  10.0);
    } // end floor
    
    // reset the color, just in case
    glColor3f(1.0, 1.0, 1.0);
  glEnd();
}

// Draw the posture of the specified frame
void BVH::RenderFigure(int frame_no, float scale) {
  // Draw by specifying the BVH skeleton / posture
  RenderFigure(joints[0], motion + frame_no * num_channel, scale);
}

// Draw the specified BVH skeleton / posture (class function)
void BVH::RenderFigure(const Joint *joint, const double *data, float scale) {
  glPushMatrix();

  // Apply translation for root joints
  if (joint->parent == NULL) {
    // push new matrix so we translate only that which we want to
    glPushMatrix();
    // translate by root position, draw axes, remove translation
    //glTranslatef(data[0] * scale, data[1] * scale, data[2] * scale); // original translation meant for root
    glTranslatef(-data[0] * scale, -data[1] * scale, -data[2] * scale);
    RenderAxes();
    // this means that if the root should be at X, Y, Z while the axes represent 0,0,0,
    // with this translation the axes are at -X, -Y, -Z with the root at 0,0,0
    // so the offset of the root from the axes is correct even if the axes are not at the literal origin
    glPopMatrix();
    //glTranslatef(data[0] * scale, data[1] * scale, data[2] * scale);
  }
  // For child joints, apply translation from the parent joint
  else {
    glTranslatef(joint->offset[0] * scale, joint->offset[1] * scale,
                 joint->offset[2] * scale);
  }

  // Apply rotation from parent joint (rotation from world coordinates for root
  // joint)
  int i;
  for (i = 0; i < joint->channels.size(); i++) {
    Channel *channel = joint->channels[i];
    if (channel->type == X_ROTATION)
      glRotatef(data[channel->index] + joint->mod[0], 1.0f, 0.0f, 0.0f);
    else if (channel->type == Y_ROTATION)
      glRotatef(data[channel->index] + joint->mod[1], 0.0f, 1.0f, 0.0f);
    else if (channel->type == Z_ROTATION)
      glRotatef(data[channel->index] + joint->mod[2], 0.0f, 0.0f, 1.0f);
  }

  // Draw a link
  // Draw a link from the origin to the end of the joint coordinate system
  if (joint->children.size() == 0) {
    RenderBone(0.0f, 0.0f, 0.0f, joint->site[0] * scale, joint->site[1] * scale,
               joint->site[2] * scale);
  }
  // Draw a link from the origin of the joint coordinate system to the
  // connection position to the next joint

  // Draw a cylinder from the center point to the connection position to all
  // joints to the connection position to each joint
  if (joint->children.size() >= 1) {
    // Calculate the center point to the origin and the connection position to
    // all joints
    float center[3] = {0.0f, 0.0f, 0.0f};
    for (i = 0; i < joint->children.size(); i++) {
      Joint *child = joint->children[i];
      center[0] += child->offset[0];
      center[1] += child->offset[1];
      center[2] += child->offset[2];
    }
    center[0] /= joint->children.size() + 1;
    center[1] /= joint->children.size() + 1;
    center[2] /= joint->children.size() + 1;

    // Draw a link from the origin to the center point
    RenderBone(0.0f, 0.0f, 0.0f, center[0] * scale, center[1] * scale,
               center[2] * scale);

    // Draw a link from the center point to the connection position to the next
    // joint
    for (i = 0; i < joint->children.size(); i++) {
      Joint *child = joint->children[i];
      RenderBone(center[0] * scale, center[1] * scale, center[2] * scale,
                 child->offset[0] * scale, child->offset[1] * scale,
                 child->offset[2] * scale);
      // setting joint highlight 
      int hightlightMode = 0;
      if (joint_highlight_enabled) {
        // even tho we draw the child set highlight as if we are drawing the joint bc this works
        if (selectedJoint == joint) hightlightMode = 2;
        else if (selectedJoint == child) hightlightMode = 1;
        else if (selectedJoint->parent == child) hightlightMode = -1;
        if (highlightedJoint == child) hightlightMode = 3;
        else if (isControl(child)) hightlightMode = 4;
      }
      RenderJoint(child->offset[0] * scale, child->offset[1] * scale, child->offset[2] * scale, hightlightMode);
    }
  }

  // Recursive call to the child joint
  for (i = 0; i < joint->children.size(); i++) {
    RenderFigure(joint->children[i], data, scale);
  }

  glPopMatrix();
}

// Draw a single link in the BVH skeleton (class function)
void BVH::RenderBone(float x0, float y0, float z0, float x1, float y1, float z1,
                     float bRadius) {
  // Draw a cylinder connecting the given two points

  // Convert the information of the two end points of the cylinder to the
  // information of the origin, orientation, and length
  GLdouble dir_x = x1 - x0;
  GLdouble dir_y = y1 - y0;
  GLdouble dir_z = z1 - z0;
  GLdouble bone_length = sqrt(dir_x * dir_x + dir_y * dir_y + dir_z * dir_z);

  // Setting drawing parameters
  static GLUquadricObj *quad_obj = NULL;
  if (quad_obj == NULL) quad_obj = gluNewQuadric();
  gluQuadricDrawStyle(quad_obj, GLU_FILL);
  gluQuadricNormals(quad_obj, GLU_SMOOTH);

  glPushMatrix();

  // Set translation
  glTranslated(x0, y0, z0);

  // Below, calculate the matrix that represents the rotation of the cylinder

  // Normalize the z-axis to the unit vector
  double length;
  length = sqrt(dir_x * dir_x + dir_y * dir_y + dir_z * dir_z);
  if (length < 0.0001) {
    dir_x = 0.0;
    dir_y = 0.0;
    dir_z = 1.0;
    length = 1.0;
  }
  dir_x /= length;
  dir_y /= length;
  dir_z /= length;

  // Set the reference y-axis orientation
  GLdouble up_x, up_y, up_z;
  up_x = 0.0;
  up_y = 1.0;
  up_z = 0.0;

  // Calculate the x-axis direction from the outer product of the z-axis and
  // y-axis
  double side_x, side_y, side_z;
  side_x = up_y * dir_z - up_z * dir_y;
  side_y = up_z * dir_x - up_x * dir_z;
  side_z = up_x * dir_y - up_y * dir_x;

  // Normalize x-axis to unit vector
  length = sqrt(side_x * side_x + side_y * side_y + side_z * side_z);
  if (length < 0.0001) {
    side_x = 1.0;
    side_y = 0.0;
    side_z = 0.0;
    length = 1.0;
  }
  side_x /= length;
  side_y /= length;
  side_z /= length;

  // Calculate the y-axis orientation from the outer product of the z-axis and
  // x-axis
  up_x = dir_y * side_z - dir_z * side_y;
  up_y = dir_z * side_x - dir_x * side_z;
  up_z = dir_x * side_y - dir_y * side_x;

  // Set the rotation matrix
  GLdouble m[16] = {side_x, side_y, side_z, 0.0, up_x, up_y, up_z, 0.0,
                    dir_x,  dir_y,  dir_z,  0.0, 0.0,  0.0,  0.0,  1.0};
  glMultMatrixd(m);

  // Cylinder settings
  GLdouble radius = bRadius;  // Cylinder thickness
  GLdouble slices = 8.0;      // Radial fractions of the cylinder (default 12)
  GLdouble stack = 3.0;  // Subdivision of round slices of cylinder (default 1)

  // Draw a cylinder
  // set color
  glColor3f(0.5,0.5,0.5);
  gluCylinder(quad_obj, radius, radius, bone_length, slices, stack);

  glPopMatrix();
}

// Draw a single link in the BVH skeleton (class function)
void BVH::RenderJoint(float x0, float y0, float z0, int highlight_mode,
                     float bRadius) {
  // Draw a sphere at the given point


  // Setting drawing parameters
  static GLUquadricObj *quad_obj = NULL;
  if (quad_obj == NULL) quad_obj = gluNewQuadric();
  gluQuadricDrawStyle(quad_obj, GLU_FILL);
  gluQuadricNormals(quad_obj, GLU_SMOOTH);

  glPushMatrix();

  // Set translation
  glTranslated(x0, y0, z0);

  // sphere settings
  GLdouble radius = bRadius;  // sphere radius
  GLdouble slices = 8.0;      // Radial fractions of the cylinder (default 12)
  GLdouble stack = 5.0;  // Subdivision of round slices of cylinder (default 1)

  // Draw a sphere
  // set color to be yellow
  glColor3f(0.6, 0.6, 0.1);

  float selectedColor[3] = {0.2, 0.7, 0.2};
  float controlColor[3] = {0.8, 0.8, 0.3};
  float highlightedColor[3] = {0.8, 0.8, 0.8};
  float childColor[3] = {0.0, 0.4, 0.9};
  float parentColor[3] = {0.5, 0.0, 0.9};
  float noColor[3] = {0.0, 0.0, 0.0};
  if (highlight_mode == 1) glColor3fv(selectedColor);
  else if (highlight_mode == 3) glColor3fv(highlightedColor);
  else if (highlight_mode == 4) glColor3fv(controlColor);
  else if (highlight_mode == 2) glColor3fv(childColor);
  else if (highlight_mode == -1) glColor3fv(parentColor);

  gluSphere(quad_obj, radius, slices, stack);
  glColor3fv(noColor);

  glPopMatrix();
}

// End of BVH.cpp